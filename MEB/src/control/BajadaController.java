package control;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import logica.BajadaLogic;
import logica.MetodosDeLogica;
import pojos.Opcion;
import pojos.Papel;
import util.Constantes;

public class BajadaController {
	
	public static List<Papel> getListaPapeles(){

		List<Papel> listaPapeles = new ArrayList<Papel> ();
		
		//SIMBOLOS=APBR:52.5-PAMP:7.25
		String[] simboloProperties = PropertiesController.SIMBOLOS.split("-");
		String[] valoresSimbolo = null;
		Papel vPapel = null;
		
		for(String valor: simboloProperties){
			valoresSimbolo = valor.split(":");
			vPapel = new Papel();
			vPapel.setSimbolo_papel(valoresSimbolo[0]);
			vPapel.setPrecio_papel(Double.parseDouble(valoresSimbolo[1]));
			listaPapeles.add(vPapel);
		}
		return listaPapeles;
	}
	
	public static Map<String,String> getMapaStrikeExcepciones(){

		Map <String,String> mapaExcepciones = new HashMap <String,String>();
		
		//GGAL:2-TS:3
		String[] excepcionesProperties = PropertiesController.STRIKE_DETECT_EXCEPCIONES.split("-");
		String[] valores = null;
		
		for(String valor: excepcionesProperties){
			valores = valor.split(":");
			mapaExcepciones.put(valores[0], valores[1]);
		}
		return mapaExcepciones;
	}
	
	public static List<Papel> getListaPapeles_for_Rangos(){

		List<Papel> listaPapeles = new ArrayList<Papel> ();
		Papel vPapel = null;
		
		//papel:YPFD-precio:364-basePapel:YPFC380-resistencia:0.0-soporte:0.0
		if(PropertiesController.SIMBOLO_R1 != null){
			String[] cadena_R1 = PropertiesController.SIMBOLO_R1.split("-");
				
			vPapel = new Papel();
			vPapel.setSimbolo_papel((cadena_R1[0].split(":"))[1]);
			vPapel.setPrecio_papel(Double.parseDouble((cadena_R1[1].split(":"))[1]));
			vPapel.setBase_papel((cadena_R1[2].split(":"))[1]);
			vPapel.setPrecio_resistencia(Double.parseDouble((cadena_R1[3].split(":"))[1]));
			vPapel.setPrecio_soporte(Double.parseDouble((cadena_R1[4].split(":"))[1]));
			
			listaPapeles.add(vPapel);
		}
		
		if(PropertiesController.SIMBOLO_R2 != null){
			String[] cadena_R2 = PropertiesController.SIMBOLO_R2.split("-");
				
			vPapel = new Papel();
			vPapel.setSimbolo_papel((cadena_R2[0].split(":"))[1]);
			vPapel.setPrecio_papel(Double.parseDouble((cadena_R2[1].split(":"))[1]));
			vPapel.setBase_papel((cadena_R2[2].split(":"))[1]);
			vPapel.setPrecio_resistencia(Double.parseDouble((cadena_R2[3].split(":"))[1]));
			vPapel.setPrecio_soporte(Double.parseDouble((cadena_R2[4].split(":"))[1]));
			
			listaPapeles.add(vPapel);
		}
		
		if(PropertiesController.SIMBOLO_R3 != null){
			String[] cadena_R3 = PropertiesController.SIMBOLO_R3.split("-");
				
			vPapel = new Papel();
			vPapel.setSimbolo_papel((cadena_R3[0].split(":"))[1]);
			vPapel.setPrecio_papel(Double.parseDouble((cadena_R3[1].split(":"))[1]));
			vPapel.setBase_papel((cadena_R3[2].split(":"))[1]);
			vPapel.setPrecio_resistencia(Double.parseDouble((cadena_R3[3].split(":"))[1]));
			vPapel.setPrecio_soporte(Double.parseDouble((cadena_R3[4].split(":"))[1]));
			
			listaPapeles.add(vPapel);
		}
		
		if(PropertiesController.SIMBOLO_R4 != null){
			String[] cadena_R4 = PropertiesController.SIMBOLO_R4.split("-");
				
			vPapel = new Papel();
			vPapel.setSimbolo_papel((cadena_R4[0].split(":"))[1]);
			vPapel.setPrecio_papel(Double.parseDouble((cadena_R4[1].split(":"))[1]));
			vPapel.setBase_papel((cadena_R4[2].split(":"))[1]);
			vPapel.setPrecio_resistencia(Double.parseDouble((cadena_R4[3].split(":"))[1]));
			vPapel.setPrecio_soporte(Double.parseDouble((cadena_R4[4].split(":"))[1]));
			
			listaPapeles.add(vPapel);
		}
		
		if(PropertiesController.SIMBOLO_R5 != null){
			String[] cadena_R5 = PropertiesController.SIMBOLO_R5.split("-");
				
			vPapel = new Papel();
			vPapel.setSimbolo_papel((cadena_R5[0].split(":"))[1]);
			vPapel.setPrecio_papel(Double.parseDouble((cadena_R5[1].split(":"))[1]));
			vPapel.setBase_papel((cadena_R5[2].split(":"))[1]);
			vPapel.setPrecio_resistencia(Double.parseDouble((cadena_R5[3].split(":"))[1]));
			vPapel.setPrecio_soporte(Double.parseDouble((cadena_R5[4].split(":"))[1]));
			
			listaPapeles.add(vPapel);
		}
		
		return listaPapeles;
	}
	
	public static List<Opcion> ejecutaBajadaCotizacionesOpcionesCall_by_Papel(Papel papel){
		String rutaArchivo = MetodosDeLogica.getRUTA_CARPETA_OPCIONES() + papel.getSimbolo_papel()+ ".html";
		final List<Opcion> listaOpciones = BajadaLogic.procesaHTML_from_file(rutaArchivo, Constantes.CALL_CHAR);	
		return listaOpciones;
	}
	
	public static List<Opcion> ejecutaBajadaCotizacionesOpcionesPut_by_Papel(Papel papel){
		String rutaArchivo = MetodosDeLogica.getRUTA_CARPETA_OPCIONES() + papel.getSimbolo_papel()+ ".html";
		final List<Opcion> listaOpciones = BajadaLogic.procesaHTML_from_file(rutaArchivo,Constantes.PUT_CHAR);	
		return listaOpciones;
	}



/*	
	public static boolean login_app(){
		
		Connection.Response res;

		try {
			res = Jsoup
			        .connect(IOL_Login)
			        .userAgent("Mozilla/5.0").execute();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		try {
			Document doc2 = Jsoup.connect(IOL_Login)
			.userAgent("Mozilla/5.0")
			.cookies(res.cookies())
			.data("Username", usr_JFM)
			.data("Password", pw_JFM)
			.data("submit", "")
			.post();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	
		return true;
	}
*/	
	
	
}
