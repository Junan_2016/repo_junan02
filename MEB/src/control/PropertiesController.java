package control;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesController {

	static private Properties vProperties; 
	
	public static String SIMBOLOS,RUTA_CARPETA_OPCIONES,SIMBOLO_R1,SIMBOLO_R2,SIMBOLO_R3,SIMBOLO_R4,SIMBOLO_R5,
						 STRIKE_DETECT_EXCEPCIONES,MES_BASE,URL_DOWNLOAD_HTML,TIPO_ESPECIE,INV_ACTUAL_1,INV_ACTUAL_2,NO_MOSTRAR_PROMEDIOS = null;
	
    static {
    	
    	try {
    		InputStream is = new PropertiesController().getClass().getResourceAsStream("/MEB.properties");
    		vProperties = new Properties();
    		vProperties.load(is);
    		
    		cargaMEB();
    		
            readPositionsPropertiesByLine();
    		
            //cargarMEBposiciones();
    		
	    } catch (IOException e) {
			System.err.println("Error al cargar el archivo de propiedades MEB.properties");
		}

    }
    
    
    /**
     * 
     */
    private static void readPositionsPropertiesByLine() {
    	
    	BufferedReader reader = null;
    	int flag_INV_ACTUAL_1 = 1;
    	
        try {
            String filePath = new File("").getAbsolutePath();
            //System.out.println (filePath);
    		
    		reader = new BufferedReader(new FileReader(filePath + "/MEB_posiciones.properties"));
                       
            String line = null;  
            
            while ((line = reader.readLine()) != null){
            	
                if (!(line.startsWith("#"))){    
                	
    	            if (line.contains("INV_ACTUAL_2")){
    	            	flag_INV_ACTUAL_1 = 0;
    	            }                	                	
    	            if (flag_INV_ACTUAL_1 == 1 && !line.contains("INV_ACTUAL_1")){
    	            	
	            		if (INV_ACTUAL_1 == null)	            			
	            			INV_ACTUAL_1 = line;
	            		else 
	            			INV_ACTUAL_1 = INV_ACTUAL_1 + line;    	            		
    	            }       
    	            if (flag_INV_ACTUAL_1 == 0 && !line.contains("INV_ACTUAL_2")){
	            		
    	            	if (INV_ACTUAL_2 == null)	            			
	            			INV_ACTUAL_2 = line;
	            		else 
	            			INV_ACTUAL_2 = INV_ACTUAL_2 + line;  
    	            }
                    //System.out.println(line);

                }
            }
            //System.out.println("INV_ACTUAL_1: " + INV_ACTUAL_1);
            //System.out.println("INV_ACTUAL_2: " + INV_ACTUAL_2);
            //System.out.println("********************************");
            
        } catch (FileNotFoundException e1) {
        	System.err.println("Error al obtener propiedades MEB_posiciones.properties.");
        }
         catch (IOException e) {
        	 System.err.println("Error al obtener propiedades MEB_posiciones.properties.");
        } finally {
            try {
            	reader.close();
            } catch (IOException e) {
            	System.err.println("Error al obtener propiedades MEB_posiciones.properties.");
            }
        }
    }
	 
    
    /**
     * 
     */
    private static void cargaMEB(){
    	
    	try {
        	SIMBOLOS = vProperties.getProperty("SIMBOLOS");
        	RUTA_CARPETA_OPCIONES = vProperties.getProperty("RUTA_CARPETA_OPCIONES");
        		
        	SIMBOLO_R1 = vProperties.getProperty("SIMBOLO_R1");
        	SIMBOLO_R2 = vProperties.getProperty("SIMBOLO_R2");
        	SIMBOLO_R3 = vProperties.getProperty("SIMBOLO_R3");
        	SIMBOLO_R4 = vProperties.getProperty("SIMBOLO_R4");
        	SIMBOLO_R5 = vProperties.getProperty("SIMBOLO_R5");
        		
        	STRIKE_DETECT_EXCEPCIONES = vProperties.getProperty("STRIKE_DETECT_EXCEPCIONES");
        	
        	MES_BASE = vProperties.getProperty("MES_BASE");
        	
        	URL_DOWNLOAD_HTML = vProperties.getProperty("URL_DOWNLOAD_HTML");
        	TIPO_ESPECIE = vProperties.getProperty("TIPO_ESPECIE");
        	
        	NO_MOSTRAR_PROMEDIOS = vProperties.getProperty("NO_MOSTRAR_PROMEDIOS");
        	
	    } catch (Exception e) {
			System.err.println("Error al obtener propiedades MEB.properties.");
		}
	
    		
    }	
 
    
	/**
     * 
     * @return
     */
	public static String get_MES_BASE(){
		if(MES_BASE != null)
			return MES_BASE;		
		return null;
	}

	
}

