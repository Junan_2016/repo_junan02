package control;

import java.util.ArrayList;
import java.util.List;

import pojos.MejorLanzamiento;
import pojos.Opcion;
import pojos.Papel;
import logica.CallLogic;

public class MejorLanzamientoController {
		
	public static void mostrarMejorLanzamiento() {
		
		List<Papel> listaPapeles = BajadaController.getListaPapeles();
		if (listaPapeles.size()<=0){
			System.err.println("listaPapeles vacia");
			return;
		}
		
		for(Papel vPapel : listaPapeles ){
			
			List<Opcion> listaOpciones = BajadaController.ejecutaBajadaCotizacionesOpcionesCall_by_Papel(vPapel);
			if (listaOpciones.size() == 0){
				return;
			}	
	
			CallLogic callLogic = new CallLogic();
			callLogic.eliminarCallsCero(listaOpciones);
			
			if (listaOpciones.size() == 0){
				return;
			}
			
			ArrayList<MejorLanzamiento> listaMejoresLanzamientos = callLogic.calcularBeneficio(listaOpciones, vPapel);
			callLogic.calcularPorcentajeBeneficio(listaMejoresLanzamientos);
			callLogic.ordenarPorcentajeBeneficio(listaMejoresLanzamientos);
			callLogic.mostrarResultados(listaMejoresLanzamientos);
			
			System.out.println("----------------------------------------------------------------------------------");
		}
	
	}

}
