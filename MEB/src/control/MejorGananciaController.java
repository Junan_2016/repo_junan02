package control;

import java.util.ArrayList;
import java.util.List;
import pojos.MejorLanzamiento;
import pojos.Opcion;
import pojos.Papel;
import logica.MejorGananciaLogic;

public class MejorGananciaController {
		
	public static void mostrarMejorCall() {
		
		List<Papel> listaPapeles = BajadaController.getListaPapeles();
		if (listaPapeles.size()<=0){
			System.err.println("listaPapeles vacia");
			return;
		}
		
		for(Papel vPapel : listaPapeles ){

			//Lectura de calls
			List<Opcion> listaOpcionesCalls =  BajadaController.ejecutaBajadaCotizacionesOpcionesCall_by_Papel(vPapel);
			if (listaOpcionesCalls.size() == 0){
				System.err.println("(listaOpcionesCalls.size()= 0");
				return;
			}	
			
			//limpieza de calls
			MejorGananciaLogic.eliminarCallCero_Qventa(listaOpcionesCalls);
			if (listaOpcionesCalls.size() == 0){
				System.err.println("listaOpcionesCalls.size()=0");
				return;
			}
			
			//logica
			ArrayList<MejorLanzamiento> listaMejoresGanancias = MejorGananciaLogic.calcularMejorGanancia(listaOpcionesCalls, vPapel);
			MejorGananciaLogic.ordenarPorcentajeBeneficio(listaMejoresGanancias);
			MejorGananciaLogic.mostrarResultados(listaMejoresGanancias);
			
			System.out.println();
		}
	}


}
