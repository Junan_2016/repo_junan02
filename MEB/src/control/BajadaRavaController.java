package control;


import java.util.ArrayList;
import java.util.List;

import logica.BajadaRavaLogic;
import logica.InversionEspecieLogic;
import pojos.Especie;
import pojos.InversionEspecie;


public class BajadaRavaController {
	
	static List<Especie> listaEspeciesTemp = null;
	static List<Especie> listaEspecies = new ArrayList<Especie> ();
	static List<InversionEspecie> listaInversionesEspecie = null;
	
	
	/**
	 * 
	 */
	public static void obtenerPanelDeControl() { 
		
		String invActual_1 = PropertiesController.INV_ACTUAL_1;
		String invActual_2 = PropertiesController.INV_ACTUAL_2;
		String nombrePropiedadInvActual = null;
		
		obtenerEspecies();		
		
		if (invActual_1 != null || invActual_1 == "") {
			nombrePropiedadInvActual = "INV_ACTUAL_1";
			obtenerInversiones(invActual_1,nombrePropiedadInvActual);
		}
		
		if (invActual_2 != null && !invActual_2.equals("")) {
			nombrePropiedadInvActual = "INV_ACTUAL_2";
			obtenerInversiones(invActual_2,nombrePropiedadInvActual);
		}
			
	}

	
	/**
	 * 
	 */
	public static void obtenerEspecies() {
		
		String[] tipoEspeciesProperties = PropertiesController.TIPO_ESPECIE.split(",");
		
		for(String especie: tipoEspeciesProperties){
			
			listaEspeciesTemp = BajadaRavaLogic.procesaHTML_from_file(especie);
			if (listaEspeciesTemp.size()<=0){
				System.err.println("lista especie: " + especie + "está vacía.");
				return;
			}
			else{
				listaEspecies.addAll(listaEspeciesTemp);
			}
			
			/*
			System.out.println("------------------------ ESPECIE:" + especie + " ----------------------------");
			for(Especie vEspecie : listaEspecies ){

				System.out.println(vEspecie.getSimbolo_especie() + " - " + vEspecie.getPrecio_especie());
				System.out.println("-----------------------------------------------------------------");
			}	
			System.out.println("--------------- Bajada especies:" + especie + " OK --------------------------");
			*/
		}
		//System.out.println("-----------------------------------------------------------------");
				
	}

	
	/**
	 * @param invActual 
	 * @param nombrePropiedadInvActual 
	 * 
	 */
	public static void obtenerInversiones(String invActual, String nombrePropiedadInvActual) {
		
		try {
			
			listaInversionesEspecie = InversionEspecieLogic.getInversionesEspecie(invActual);	
			
			if (listaInversionesEspecie != null){
				
				System.out.println(nombrePropiedadInvActual);
				InversionEspecieLogic.verPanelDeControlInversiones(listaInversionesEspecie, listaEspecies);
			}	
		
		} catch (Exception e) {			
			System.err.println("Error Properties " + nombrePropiedadInvActual + " es nula");
			//e.printStackTrace();
		}
		
	}
	
}
