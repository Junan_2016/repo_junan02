package control;

import java.util.List;

import logica.EstrategiaLogic;
import pojos.Opcion;
import pojos.Papel;

public class EstrategiaController {

	public static void mostrarMejorEstrategia() {
		
		List<Papel> listaPapeles = BajadaController.getListaPapeles();
		if (listaPapeles.size()<=0){
			System.err.println("listaPapeles vacia");
			return;
		}
		
		for(Papel vPapel : listaPapeles ){

			List<Opcion> listaCalls = BajadaController.ejecutaBajadaCotizacionesOpcionesCall_by_Papel(vPapel);
			List<Opcion> listaPuts = BajadaController.ejecutaBajadaCotizacionesOpcionesPut_by_Papel(vPapel);
			
			if (listaCalls.size() == 0 || listaPuts.size() == 0){
				System.err.println("Listas C_ o P_ nulas");
				return;
			}	
			
			String mes = obtenerMes();
			
			EstrategiaLogic.generarEstrategia(listaCalls,listaPuts,vPapel,mes);
	
			System.out.println("------------------------------------------------------------------------------------");
			
		}
	}

	private static String obtenerMes() {
		
		if(PropertiesController.get_MES_BASE() != null)
			  return PropertiesController.get_MES_BASE();
			
		return null;
	}
}
