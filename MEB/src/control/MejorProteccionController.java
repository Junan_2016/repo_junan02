package control;

import java.util.ArrayList;
import java.util.List;

import pojos.MejorLanzamiento;
import pojos.Opcion;
import pojos.Papel;
import logica.MejorProteccionLogic;


public class MejorProteccionController {
		
	public static void mostrarMejorPut() {
		
		List<Papel> listaPapeles = BajadaController.getListaPapeles();
		if (listaPapeles.size()<=0){
			System.err.println("listaPapeles vacia");
			return;
		}
		
		for(Papel vPapel : listaPapeles ){

			//Lectura de puts
			List<Opcion> listaOpcionesPuts =  BajadaController.ejecutaBajadaCotizacionesOpcionesPut_by_Papel(vPapel);
			if (listaOpcionesPuts.size() == 0){
				System.err.println("(listaOpcionesPuts.size()= 0");
				return;
			}	
			
			//limpieza de puts
			MejorProteccionLogic.eliminarPutCero_QVenta(listaOpcionesPuts);
			if (listaOpcionesPuts.size() == 0){
				System.err.println("listaOpcionesPuts.size()=0");
				return;
			}
			
			//logica
			ArrayList<MejorLanzamiento> listaMejoresProtecciones = MejorProteccionLogic.calcularMejorProteccion(listaOpcionesPuts, vPapel);
			MejorProteccionLogic.ordenarPorcentajeBeneficio(listaMejoresProtecciones);
			MejorProteccionLogic.mostrarResultados(listaMejoresProtecciones);
			
			System.out.println();
		}
	}


}
