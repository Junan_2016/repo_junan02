package control;

import java.util.ArrayList;
import java.util.List;

import logica.CallLogic;
import logica.ChequeoLanzamientoLogic;
import pojos.Opcion;
import pojos.OpcionLanzada;
import pojos.Papel;

public class RangosController {
	
	public static void chequearRangos() {
		
		List<Papel> listaPapeles = BajadaController.getListaPapeles_for_Rangos();
		if (listaPapeles.size()<=0){
			System.err.println("listaPapeles vacia");
			return;
		}
		
		for(Papel vPapel : listaPapeles ){

			List<Opcion> listaCalls = BajadaController.ejecutaBajadaCotizacionesOpcionesCall_by_Papel(vPapel);
			List<Opcion> listaPuts = BajadaController.ejecutaBajadaCotizacionesOpcionesPut_by_Papel(vPapel);
			
			if (listaCalls.size() == 0 || listaPuts.size() == 0){
				System.err.println("Listas C_ o P_ nulas");
				return;
			}
						
			CallLogic callLogic = new CallLogic();
			callLogic.eliminarCallsCero(listaCalls);
			
			if (listaCalls.size() == 0){
				System.err.println("listaCalls nulas");
				return;
			}
			
			//1. Revisar estados de soporte y resistencia
			ChequeoLanzamientoLogic chequeoLanzamientoLogic = new ChequeoLanzamientoLogic();
			chequeoLanzamientoLogic.chequearEstados(vPapel);
			
			OpcionLanzada vOpcionLanzada = new OpcionLanzada();
			vOpcionLanzada.setBase(vPapel.getBase_papel());
			vOpcionLanzada.setMes_lanzamiento(PropertiesController.get_MES_BASE());
						
			//2.1. Recalculando RESISTENCIA
			ArrayList<Opcion> opcionesSeleccionadas = chequeoLanzamientoLogic.seleccionarBasesMes(listaCalls, vOpcionLanzada);
			
			Opcion opcionStrikeMenor = chequeoLanzamientoLogic.seleccionarStrikeMenor(opcionesSeleccionadas, vPapel);
			Opcion opcionStrikeMayor = chequeoLanzamientoLogic.seleccionarStrikeMayor(opcionesSeleccionadas, vPapel);
			
			chequeoLanzamientoLogic.recalcularRango(opcionStrikeMenor,opcionStrikeMayor, true);
			
			
			//2.2. Recalculando SOPORTE
			ArrayList<Opcion> putsSeleccionadas = chequeoLanzamientoLogic.seleccionarBasesMes(listaPuts, vOpcionLanzada);
			
			opcionStrikeMenor = chequeoLanzamientoLogic.seleccionarStrikeMenor(putsSeleccionadas, vPapel);
			opcionStrikeMayor = chequeoLanzamientoLogic.seleccionarStrikeMayor(putsSeleccionadas, vPapel);
			
			chequeoLanzamientoLogic.recalcularRango(opcionStrikeMenor,opcionStrikeMayor, false);
			
			//3. Recompra de Call		
			//chequeoLanzamientoLogic.recomprarCall(listaOpcionesCall, opcionLanzada);
			
			System.out.println("-----------------------------------------------------------------");
		}		
				
	}



	


	


	

		
	
}
