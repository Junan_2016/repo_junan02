package control;

import java.util.List;

import logica.CYP_Logic;
import pojos.Opcion;
import pojos.Papel;

public class CYPController {

	public static void obtener_CYP() {
		
		List<Papel> listaPapeles = BajadaController.getListaPapeles();
		if (listaPapeles.size()<=0){
			System.err.println("listaPapeles vacia");
			return;
		}
		
		for(Papel vPapel : listaPapeles ){

			List<Opcion> listaCalls = BajadaController.ejecutaBajadaCotizacionesOpcionesCall_by_Papel(vPapel);
			List<Opcion> listaPuts = BajadaController.ejecutaBajadaCotizacionesOpcionesPut_by_Papel(vPapel);

			if (listaCalls.size() == 0 && listaPuts.size() == 0){
				System.err.println(vPapel.getSimbolo_papel() + "con Listas nulas");
				return;
			}
			
			if (listaCalls.size() == 0){
				System.err.println(vPapel.getSimbolo_papel() + " tiene C_ nulas");
			}	

			if (listaPuts.size() == 0){
				System.err.println(vPapel.getSimbolo_papel() + " tiene P_ nulas");
			}	
		
			String mes = obtenerMes();
			
			CYP_Logic.generar_CYP(listaCalls,listaPuts,vPapel,mes);
	
			System.out.println("------------------------------------------------------------------------------------");
			
		}
	}

	private static String obtenerMes() {
		
		if(PropertiesController.get_MES_BASE() != null)
		  return PropertiesController.get_MES_BASE();
		
		return null;
	}
}
