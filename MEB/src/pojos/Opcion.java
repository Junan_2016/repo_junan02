package pojos;

public class Opcion {

	private String base = null;
	private double strike = 0;
	private int cant_compra = 0;  //para adquirir en el super
	private int cant_venta = 0;   //para soltarlas en el super
	private double precio_compra = 0;
	private double precio_venta = 0;
	private char tipo = ' ';  // C-Call o P-Put
	private String volumen;
	private String porcMKT;
		
	public Opcion() {
		super();
	}
		
	public String getBase() {
		return base;
	}
	
	public void setBase(String base) {
		this.base = base;
	}
		
	public double getStrike() {
		return strike;
	}
	public void setStrike(double strike) {
		this.strike = strike;
	}
		
	public int getCant_compra() {
		return cant_compra;
	}

	public void setCant_compra(int cant_compra) {
		this.cant_compra = cant_compra;
	}
	
	public int getCant_venta() {
		return cant_venta;
	}
	public void setCant_venta(int cant_venta) {
		this.cant_venta = cant_venta;
	}
	
	public double getPrecio_compra() {
		return precio_compra;
	}
	public void setPrecio_compra(double precio_compra) {
		this.precio_compra = precio_compra;
	}
	
	public double getPrecio_venta() {
		return precio_venta;
	}
	public void setPrecio_venta(double precio_venta) {
		this.precio_venta = precio_venta;
	}
	
	public char getTipo() {
		return tipo;
	}
	public void setTipo(char tipo) {
		this.tipo = tipo;
	}

	public String getVolumen() {
		return volumen;
	}

	public void setVolumen(String volumen) {
		this.volumen = volumen;
	}

	public String getPorcMKT() {
		return porcMKT;
	}

	public void setPorcMKT(String porcMKT) {
		this.porcMKT = porcMKT;
	}
	
}
