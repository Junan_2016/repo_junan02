package pojos;

public class Derivado{
	
	private Opcion opcion = null; //necesitamos: base y precio_compra
	private Papel papel = null; //necesitamos: precio_papel
	private double beneficio_bruto = 0;
	private double porcentaje_beneficio = 0;
	
	public Derivado () {
		super();
	}
	
	public Derivado(Opcion opcion, Papel papel, double beneficio_bruto,	double porcentaje_beneficio) {
		super();
		this.opcion = opcion;
		this.papel = papel;
		this.beneficio_bruto = beneficio_bruto;
		this.porcentaje_beneficio = porcentaje_beneficio;
	}

	public Opcion getOpcion() {
		return opcion;
	}
	public void setOpcion(Opcion opcion) {
		this.opcion = opcion;
	}
	
	
	public Papel getPapel() {
		return papel;
	}
	public void setPapel(Papel papel) {
		this.papel = papel;
	}
	
	
	public double getBeneficio_bruto() {
		return beneficio_bruto;
	}
	public void setBeneficio_bruto(double beneficio_bruto) {
		this.beneficio_bruto = beneficio_bruto;
	}
	
	
	public double getPorcentaje_beneficio() {
		return porcentaje_beneficio;
	}
	public void setPorcentaje_beneficio(double porcentaje_beneficio) {
		this.porcentaje_beneficio = porcentaje_beneficio;
	}
	
}
