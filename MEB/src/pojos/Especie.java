package pojos;

import java.text.DecimalFormat;

public class Especie {
	static DecimalFormat df = new DecimalFormat("0.00");
		
	private String simbolo_especie = null;  
	private double precio_especie = 0;
	private double precio_maximo = 0;
	private double precio_minimo = 0;
	private double precio_anterior = 0;
				
	public String getFibos(){
		
		final double body = precio_maximo - precio_minimo;
		final double R_23_6 = precio_maximo - (body*0.236);		
		final double R_38_2 = precio_maximo - (body*0.382);
		final double R_50 = precio_maximo - (body*0.50);
		final double R_61_8 = precio_maximo - (body*0.618);
		final double R_76_4 = precio_maximo - (body*0.764);
		
		return
			"FIBO(intradia)->R(23.6):" + df.format(R_23_6) +
			"- R(38.2):" + df.format(R_38_2) +
			"- R(50):" + df.format(R_50) +
			"- R(61.8):" + df.format(R_61_8) +
			"- R(76.4):" + df.format(R_76_4);
	}
	
	public double getPrecio_especie() {
		return precio_especie;
	}
	public void setPrecio_especie(double precio_especie) {
		this.precio_especie = precio_especie;
	}
		
	public String getSimbolo_especie() {
		return simbolo_especie;
	}
	public void setSimbolo_especie(String simbolo_especie) {
		this.simbolo_especie = simbolo_especie;
	}

	public double getPrecio_maximo() {
		return precio_maximo;
	}
	public void setPrecio_maximo(double precio_maximo) {
		this.precio_maximo = precio_maximo;
	}
	public double getPrecio_minimo() {
		return precio_minimo;
	}
	public void setPrecio_minimo(double precio_minimo) {
		this.precio_minimo = precio_minimo;
	}
	public double getPrecio_anterior() {
		return precio_anterior;
	}
	public void setPrecio_anterior(double precio_anterior) {
		this.precio_anterior = precio_anterior;
	}
			
}
