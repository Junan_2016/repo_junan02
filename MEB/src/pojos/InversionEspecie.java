package pojos;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class InversionEspecie implements Cloneable{
	
	static DecimalFormat df = new DecimalFormat("0.00");
	static SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
	
	private String simbolo_especie = null;  //PAM
	private double precio_compra_especie = 0;
	private double precio_actual = 0;
	private double precio_maximo = 0;
	private int cantidad_compra_especie = 0;
	private double total_compra_y_comision = 0;
	private Calendar fecha_compra_especie = null;
	
	private double ganancia_bruta = 0;
	private double porcentaje_ganancia_bruta = 0;
	private double ganancia_neta = 0;
	private double porcentaje_ganancia = 0;
	
	private double comision = 0;
	private double porcentaje_comision = 0;
	
	public String getSimbolo_especie() {
		return simbolo_especie;
	}
	public void setSimbolo_especie(String simbolo_especie) {
		this.simbolo_especie = simbolo_especie;
	}
	
	public double getPrecio_compra_especie() {
		return precio_compra_especie;
	}
	public void setPrecio_compra_especie(double precio_compra_especie) {
		this.precio_compra_especie = precio_compra_especie;
	}
			
	public double getPrecio_actual() {
		return precio_actual;
	}
	public void setPrecio_actual(double precio_actual) {
		this.precio_actual = precio_actual;
	}
	public int getCantidad_compra_especie() {
		return cantidad_compra_especie;
	}
	public void setCantidad_compra_especie(int cantidad_compra_especie) {
		this.cantidad_compra_especie = cantidad_compra_especie;
	}
	
	public double getTotal_compra_y_comision() {
		return total_compra_y_comision;
	}
	public void setTotal_compra_y_comision(double total_compra_y_comision) {
		this.total_compra_y_comision = total_compra_y_comision;
	}

	public Calendar getFecha_compra_especie() {
		return fecha_compra_especie;
	}
	public void setFecha_compra_especie(Calendar fecha_compra_especie) {
		this.fecha_compra_especie = fecha_compra_especie;
	}
	
	public double getPrecio_maximo() {
		return precio_maximo;
	}
	public void setPrecio_maximo(double precio_maximo) {
		this.precio_maximo = precio_maximo;
	}
	
		
	public double getGanancia_bruta() {
		return ganancia_bruta;
	}
	public void setGanancia_bruta(double ganancia_bruta) {
		this.ganancia_bruta = ganancia_bruta;
	}
	public double getPorcentaje_ganancia_bruta() {
		return porcentaje_ganancia_bruta;
	}
	public void setPorcentaje_ganancia_bruta(double porcentaje_ganancia_bruta) {
		this.porcentaje_ganancia_bruta = porcentaje_ganancia_bruta;
	}
	public double getGanancia_neta() {
		return ganancia_neta;
	}
	public void setGanancia_neta(double ganancia_neta) {
		this.ganancia_neta = ganancia_neta;
	}
	public double getPorcentaje_ganancia() {
		return porcentaje_ganancia;
	}
	public void setPorcentaje_ganancia(double porcentaje_ganancia) {
		this.porcentaje_ganancia = porcentaje_ganancia;
	}
	public double getComision() {
		return comision;
	}
	public void setComision(double comision) {
		this.comision = comision;
	}
	public double getPorcentaje_comision() {
		return porcentaje_comision;
	}
	public void setPorcentaje_comision(double porcentaje_comision) {
		this.porcentaje_comision = porcentaje_comision;
	}
	
	@Override 
	public String toString() {
	    StringBuilder result = new StringBuilder();
	    
	    result.append("Papel: " + this.simbolo_especie + "\n");
	    result.append("Precio Compra: " +  df.format(this.precio_compra_especie) + "\n");
	    result.append("Cantidad Compra: " + this.cantidad_compra_especie + "\n");
	    result.append("Total Compra: " + this.total_compra_y_comision + "\n");
	    
	    return result.toString();
	}
	
	
	/**
	 * 
	 * @return
	 */
	public String toString2() {
	    StringBuilder result = new StringBuilder();
	    
	    result.append("Papel: " + this.simbolo_especie + "\n");
	    result.append("Precio Compra: " +  df.format(this.precio_compra_especie) + "\n");
	    result.append("Precio Actual: " +  df.format(this.precio_actual) + "\n");
	    result.append("Precio Máximo: " +  df.format(this.precio_maximo) + "\n");
	    result.append("Cantidad Compra: " + this.cantidad_compra_especie + "\n");
	    result.append("Total Compra: " + this.total_compra_y_comision + "\n");
	    //result.append("Fecha Compra: " + sdf.format(this.fecha_compra_especie) + "\n");
	    
	    result.append("Comisión: " +  df.format(this.comision) + "\n");
	    result.append("% Comisión: " +  df.format(this.porcentaje_comision) + "\n");
	    
	    result.append("Ganancia_Bruta: " +  df.format(this.ganancia_bruta) + "\n");
	    result.append("Porcentaje Ganancia Bruta: " +  df.format(this.porcentaje_ganancia_bruta) + "\n");
	    result.append("Ganancia Neta: " +  df.format(this.ganancia_neta) + "\n");
	    result.append("Porcentaje Ganancia: " +  df.format(this.porcentaje_ganancia));
	     
	    return result.toString();
	}
	
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
	
}
