package pojos;

public class CombinacionCallPut extends MejorLanzamiento{
	
	private Opcion opcionCall = null;
	private Opcion opcionPut = null;
	
	
	public Opcion getOpcionCall() {
		return opcionCall;
	}

	public void setOpcionCall(Opcion opcionCall) {
		this.opcionCall = opcionCall;
	}


	public Opcion getOpcionPut() {
		return opcionPut;
	}

	public void setOpcionPut(Opcion opcionPut) {
		this.opcionPut = opcionPut;
	}
	
	

}
