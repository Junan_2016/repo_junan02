package pojos;

public class OpcionLanzada extends Opcion{
	
	private String mes_lanzamiento = null;

	
	public String getMes_lanzamiento() {
		return mes_lanzamiento;
	}

	public void setMes_lanzamiento(String mes_lanzamiento) {
		this.mes_lanzamiento = mes_lanzamiento;
	}

}
