package logica;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import pojos.MejorLanzamiento;
import pojos.Opcion;
import pojos.Papel;

public class MejorGananciaLogic {
	
	static DecimalFormat df = new DecimalFormat("0.00");
	
	public static ArrayList<MejorLanzamiento> calcularMejorGanancia(List<Opcion> listaCalls, Papel papel) {
		
		double ganancia = 0 ;
		double gananciaEfectiva = 0 ;
		ArrayList<MejorLanzamiento> listaMejoresGanancias = new ArrayList<MejorLanzamiento> ();
		MejorLanzamiento gananciaCall= null;
				
		for (Opcion option : listaCalls) {
			
			if(option.getStrike() < papel.getPrecio_papel()){
				//calculo de cobertura
				ganancia = papel.getPrecio_papel() - (option.getStrike() + option.getPrecio_venta());
				gananciaEfectiva = ganancia*100/papel.getPrecio_papel();
			} else {
				//calculo variacion
				double recorrido = option.getStrike() - papel.getPrecio_papel();
				ganancia = recorrido;
				gananciaEfectiva = recorrido*100/papel.getPrecio_papel(); //desplazamiento necesario
			}
						
			gananciaCall = new MejorLanzamiento();
			gananciaCall.setPapel(papel);
			gananciaCall.setOpcion(option);
			gananciaCall.setBeneficio_bruto(ganancia);
			gananciaCall.setPorcentaje_beneficio(gananciaEfectiva);
			listaMejoresGanancias.add(gananciaCall);
		}
		
		return listaMejoresGanancias;
	}
	
	/**
	 * Ordenamiento
	 * @param listaMejoresGanancias
	 */
	public static void ordenarPorcentajeBeneficio(List<MejorLanzamiento> listaMejoresGanancias) {
		
		Collections.sort(listaMejoresGanancias, new Comparator<MejorLanzamiento>(){

			@Override
			public int compare(MejorLanzamiento mejor1, MejorLanzamiento mejor2) {
					
				return new Double(mejor2.getPorcentaje_beneficio()).compareTo(new Double(mejor1.getPorcentaje_beneficio()));
			}
			
		});
		
	}		

	public static void mostrarResultados(ArrayList<MejorLanzamiento> listaMejoresGanancias) {
		System.out.println("MEJOR GANANCIA - " + listaMejoresGanancias.get(0).getPapel().getSimbolo_papel() + ": " + listaMejoresGanancias.get(0).getPapel().getPrecio_papel());

		String resultado = null;
		List <String> listaCobertura = new ArrayList <String>();
		List <String> listaVariacion= new ArrayList <String>();
		
		for (MejorLanzamiento mejorLanzamiento : listaMejoresGanancias) {
			
			if(mejorLanzamiento.getOpcion().getStrike() < mejorLanzamiento.getPapel().getPrecio_papel()){
				resultado = df.format(mejorLanzamiento.getPorcentaje_beneficio()) + "% (" +	
						df.format(mejorLanzamiento.getBeneficio_bruto()) + " ARS) - " + 
						mejorLanzamiento.getOpcion().getBase() +
						" - prec.venta: " + mejorLanzamiento.getOpcion().getPrecio_venta()  + 
						" - cant.venta: " + mejorLanzamiento.getOpcion().getCant_venta() +
						" - monto: " + df.format(mejorLanzamiento.getOpcion().getPrecio_venta() * mejorLanzamiento.getOpcion().getCant_venta() * 100);
				resultado = "Cobertura: " + resultado;
				listaCobertura.add(resultado);
			} else {
				resultado = df.format(mejorLanzamiento.getPorcentaje_beneficio()) +	"% (" +	
						df.format(mejorLanzamiento.getBeneficio_bruto()) + " ARS) - " + 
						mejorLanzamiento.getOpcion().getBase() +
						" - prec.venta: " + mejorLanzamiento.getOpcion().getPrecio_venta()  + 
						" - cant.venta: " + mejorLanzamiento.getOpcion().getCant_venta() +
						" - monto: " + df.format(mejorLanzamiento.getOpcion().getPrecio_venta() * mejorLanzamiento.getOpcion().getCant_venta() * 100);
				resultado = "Variacion: " + resultado;
				listaVariacion.add(resultado);
			}
		}
		
		for (String valor1 : listaCobertura) {
			System.out.println(valor1);
		}
				
		for (String valor2 : listaVariacion) {
			System.out.println(valor2);
		}
		
	}
	
	public static void eliminarCallCero_Qventa(List<Opcion> listaCalls){
		
		for (int j=0; j< listaCalls.size(); j++) {
			
			if (listaCalls.get(j).getCant_venta() == 0){
				listaCalls.remove(j);
				j=-1;
			}				
		}		
	}	
}
