package logica;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import pojos.MejorLanzamiento;
import pojos.Opcion;
import pojos.Papel;

public class MejorProteccionLogic {
	
	static DecimalFormat df = new DecimalFormat("0.00");
	
	public static ArrayList<MejorLanzamiento> calcularMejorProteccion(List<Opcion> listaPuts, Papel papel) {
		
		double proteccion = 0 ;
		double proteccionEfectiva = 0 ;
		ArrayList<MejorLanzamiento> listaMejoresProtecciones = new ArrayList<MejorLanzamiento> ();
		MejorLanzamiento proteccionPut= null;
				
		for (Opcion option : listaPuts) {
			
			proteccion = (option.getStrike()-option.getPrecio_venta())- papel.getPrecio_papel();
			proteccionEfectiva = proteccion*100/papel.getPrecio_papel();
			
			if(option.getStrike() > papel.getPrecio_papel()){
				//calculo de cobertura
				proteccion = (option.getStrike()-option.getPrecio_venta())- papel.getPrecio_papel();
				proteccionEfectiva = proteccion*100/papel.getPrecio_papel();
			} else {
				//calculo especulativo
				double restaReferencia = papel.getPrecio_papel() - option.getPrecio_venta();
				if(restaReferencia < option.getStrike())
					proteccion =  option.getPrecio_venta();
				else
					proteccion = option.getStrike() - papel.getPrecio_papel();
				
				proteccionEfectiva = proteccion*100/papel.getPrecio_papel();
			}
			
			proteccionPut = new MejorLanzamiento();
			proteccionPut.setPapel(papel);
			proteccionPut.setOpcion(option);
			proteccionPut.setBeneficio_bruto(proteccion);
			proteccionPut.setPorcentaje_beneficio(proteccionEfectiva);
			listaMejoresProtecciones.add(proteccionPut);
		}
		
		return listaMejoresProtecciones;
	}
	
	
	
	/**
	 * Ordenamiento
	 * @param listaMejoresLanzamientos
	 */
	public static void ordenarPorcentajeBeneficio(List<MejorLanzamiento> listaMejoresProtecciones) {
		
		Collections.sort(listaMejoresProtecciones, new Comparator<MejorLanzamiento>(){

			@Override
			public int compare(MejorLanzamiento mejor1, MejorLanzamiento mejor2) {
								
				return new Double(mejor2.getPorcentaje_beneficio()).compareTo(new Double(mejor1.getPorcentaje_beneficio()));
			}
			
		});
		
	}		

	public static void mostrarResultados(ArrayList<MejorLanzamiento> listaMejoresProtecciones) {
		
		System.out.println("MEJOR PROTECCCION - " + listaMejoresProtecciones.get(0).getPapel().getSimbolo_papel() + ": " + listaMejoresProtecciones.get(0).getPapel().getPrecio_papel());
		
		String resultado = null;
		List <String> listaCobertura = new ArrayList <String>();
		List <String> listaVariacion= new ArrayList <String>();
				
		for (MejorLanzamiento mejorLanzamiento : listaMejoresProtecciones) {
			
			resultado = df.format(mejorLanzamiento.getPorcentaje_beneficio()) + "% (" +	
						df.format(mejorLanzamiento.getBeneficio_bruto()) + " ARS) - " + 
						mejorLanzamiento.getOpcion().getBase() +
						" - prec.venta: " + mejorLanzamiento.getOpcion().getPrecio_venta() + 
						" - cant.venta: " + mejorLanzamiento.getOpcion().getCant_venta() +
						" - monto: " + df.format(mejorLanzamiento.getOpcion().getPrecio_venta() * mejorLanzamiento.getOpcion().getCant_venta() * 100);	
					
			if(mejorLanzamiento.getOpcion().getStrike() > mejorLanzamiento.getPapel().getPrecio_papel()){
				resultado = "Cobertura: " + resultado;
				listaCobertura.add(resultado);
			} else {
				resultado = "Variacion: " + resultado;
				listaVariacion.add(resultado);
			}
			
		}
		
		for (String valor1 : listaCobertura) {
			System.out.println(valor1);
		}
				
		for (String valor2 : listaVariacion) {
			System.out.println(valor2);
		}
	}
	
	public static void eliminarPutCero_QVenta(List<Opcion> listaPuts){
		
		for (int j=0; j< listaPuts.size(); j++) {
			
			if (listaPuts.get(j).getCant_venta() == 0){
				listaPuts.remove(j);
				j=-1;
			}				
		}		
	}	
}
