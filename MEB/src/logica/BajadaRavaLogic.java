package logica;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import control.PropertiesController;
import pojos.Especie;
import util.Validacion;


public class BajadaRavaLogic {
	
	static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
	/**
	 * Procesa HTML desde file
	 * @return
	 */
	public static List<Especie> procesaHTML_from_file(String especie){
		
		File input = null;
		Document doc = null;
		List<Especie> listaEspecies = null;
		String rutaArchivo = null;
				
		try {
				
			rutaArchivo = MetodosDeLogica.getRUTA_CARPETA_OPCIONES() + especie + "_" + formatter.format(new Date()) + ".html";
			input = new File(rutaArchivo);
			
			//if (!input.exists())
			bajarEspecies(especie);		
			
			doc = Jsoup.parse(input,null);
			listaEspecies = parseo_HTML(doc, rutaArchivo);	
			
			return listaEspecies;	
			
		} catch (IOException e) {
			System.err.println("Error al procesar el archivo: " + rutaArchivo);
			e.printStackTrace();
			return null;
			
		} catch (Exception e) {
			System.err.println("Error al parsear el archivo: " + rutaArchivo);
			e.printStackTrace();
			return null;
		}

	}

	/**
	 * 
	 */
	private static void bajarEspecies(String especie) {
		
		final String urlTemplate = PropertiesController.URL_DOWNLOAD_HTML;
		
		try {
			final String url = urlTemplate.replace("<TIPO_ESPECIE>", especie);

	 	    Document doc = Jsoup.connect(url).data("query", "Java")
	 	    		.userAgent("Mozilla").cookie("auth", "token")
	 	    		.timeout(3000).post();
 	    
	 	    guardarHTML(doc,especie);
			
		} catch (IOException e) {
			System.err.println("Error Properties URL_DOWNLOAD_HTML es nula");
			e.printStackTrace();
		}

	}

	/**
	 * Guardar web en HTML en ruta especificada
	 * @param doc
	 * @param especie
	 */
	private static void guardarHTML(Document doc, String especie) {
	   	
 	   String fileName = especie + "_" + formatter.format(new Date()) + ".html"; 
 	   String outputFile = MetodosDeLogica.getRUTA_CARPETA_OPCIONES() + fileName;
 	   
 	  try {
	 	   BufferedWriter htmlWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile), "UTF-8"));
	 	   htmlWriter.write(doc.toString());
	 	   htmlWriter.flush();
	 	   htmlWriter.close();
	 	   
		} catch (IOException e) {
			System.err.println("Error Properties RUTA_CARPETA_OPCIONES es nula");
			e.printStackTrace();
		}

	}

	
	/**
	 * Metodo de parseo de HTML
	 * @param doc
	 * @return
	 */
	private static List<Especie> parseo_HTML(Document doc, String rutaArchivo) throws Exception {
		
		List<Especie> listaEspecies = new ArrayList<Especie>();		
		Elements tablaEspecie = doc.getElementsByClass("tablapanel");		
		Elements listaTR = tablaEspecie.get(0).getElementsByTag("tr");
		
		/*
         (0) -> <td align="left"><a href="/v2/precios/panel.php?m=LID&amp;o=especie&amp;t=d">Especie</a></td> 
         (1) -> <td><a href="/v2/precios/panel.php?m=LID&amp;o=ultimo&amp;t=d">Último</a></td> 
         (2) -> <td><a href="/v2/precios/panel.php?m=LID&amp;o=variacion&amp;t=d">% Día</a></td> 
         (3) -> <td><a href="/v2/precios/panel.php?m=LID&amp;o=anterior&amp;t=d">Anterior</a></td> 
         (4) -> <td><a href="/v2/precios/panel.php?m=LID&amp;o=apertura&amp;t=d">Apertura</a></td> 
         (5) -> <td><a href="/v2/precios/panel.php?m=LID&amp;o=minimo&amp;t=d">Mínimo</a></td> 
         (6) -> <td><a href="/v2/precios/panel.php?m=LID&amp;o=maximo&amp;t=d">Máximo</a></td>  
		 */
		
		if (listaTR != null){
			
			listaTR.remove(0);			
			for (Element tr : listaTR) {
				
				Elements listaTD = tr.getElementsByTag("td");
				Especie vEspecie = new Especie();
				
				if (!listaTD.get(0).text().equalsIgnoreCase("")) 
					vEspecie.setSimbolo_especie(listaTD.get(0).text());
				
				if (!listaTD.get(1).text().equalsIgnoreCase("") && Validacion.esIdentificadorValido(listaTD.get(1).text()))
					vEspecie.setPrecio_especie(Double.valueOf(listaTD.get(1).text().replace(".","").replace(",",".")));
				
				if (!listaTD.get(1).text().equalsIgnoreCase("") && Validacion.esIdentificadorValido(listaTD.get(3).text()))
					vEspecie.setPrecio_anterior(Double.valueOf(listaTD.get(3).text().replace(".","").replace(",",".")));
		
				if (!listaTD.get(6).text().equalsIgnoreCase("") && Validacion.esIdentificadorValido(listaTD.get(5).text())) 
					vEspecie.setPrecio_minimo(Double.valueOf(listaTD.get(5).text().replace(".","").replace(",",".")));
				
				if (!listaTD.get(6).text().equalsIgnoreCase("") && Validacion.esIdentificadorValido(listaTD.get(6).text())) 
					vEspecie.setPrecio_maximo(Double.valueOf(listaTD.get(6).text().replace(".","").replace(",",".")));

				listaEspecies.add(vEspecie);
			}
		}		
		return listaEspecies;
	}


}
