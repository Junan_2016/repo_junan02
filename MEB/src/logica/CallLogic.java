package logica;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import pojos.MejorLanzamiento;
import pojos.Opcion;
import pojos.Papel;

public class CallLogic {
	
	public void eliminarCallsCero(List<Opcion> listaCalls){
		
		for (int j=0; j< listaCalls.size(); j++) {
			
			if (listaCalls.get(j).getCant_compra() == 0){
				listaCalls.remove(j);
				j=-1;
			}				
		}		
	}		

	
	public ArrayList<MejorLanzamiento> calcularBeneficio(List<Opcion> listaCalls, Papel papel) {
		
		double beneficio = 0 ;
		ArrayList<MejorLanzamiento> listaMejoresLanzamientos = new ArrayList<MejorLanzamiento> ();
		MejorLanzamiento mejorLanzamiento = null;
				
		for (Opcion option : listaCalls) {
			
			beneficio = option.getPrecio_compra() + (option.getStrike() - papel.getPrecio_papel());
			
			mejorLanzamiento = new MejorLanzamiento();
			mejorLanzamiento.setPapel(papel);
			mejorLanzamiento.setOpcion(option);
			mejorLanzamiento.setBeneficio_bruto(beneficio);
			
			listaMejoresLanzamientos.add(mejorLanzamiento);
		}
		
		return listaMejoresLanzamientos;
	}
	
	
	public void calcularPorcentajeBeneficio(ArrayList<MejorLanzamiento> listaMejoresLanzamientos) {
		
		double porcentajeDeBeneficio = 0 ;
		
		for (MejorLanzamiento mejorLanzamiento : listaMejoresLanzamientos) {
			
			porcentajeDeBeneficio = (mejorLanzamiento.getBeneficio_bruto() * 100) / mejorLanzamiento.getPapel().getPrecio_papel();
			
			mejorLanzamiento.setPorcentaje_beneficio(porcentajeDeBeneficio);
					
		}
		
	}
	
	/**
	 * Ordenamiento
	 * @param listaMejoresLanzamientos
	 */
	public void ordenarPorcentajeBeneficio(List<MejorLanzamiento> listaMejoresLanzamientos) {
		
		Collections.sort(listaMejoresLanzamientos, new Comparator<MejorLanzamiento>(){

			@Override
			public int compare(MejorLanzamiento mejor1, MejorLanzamiento mejor2) {
				
				//return new Double(mejor2.getBeneficio_bruto()).compareTo(new Double(mejor1.getBeneficio_bruto()));
				return new Double(mejor2.getOpcion().getPrecio_compra()).compareTo(new Double(mejor1.getOpcion().getPrecio_compra()));
			}
			
		});
		
	}		

	public void mostrarResultados(ArrayList<MejorLanzamiento> listaMejoresLanzamientos) {
		
		DecimalFormat df = new DecimalFormat("0.00");
		
		System.out.println("Papel: " + listaMejoresLanzamientos.get(0).getPapel().getSimbolo_papel() + 
							" - prec: " + listaMejoresLanzamientos.get(0).getPapel().getPrecio_papel() + " ARS");		
		
		for (MejorLanzamiento mejorLanzamiento : listaMejoresLanzamientos) {
			
			System.out.println("Benef: " + df.format(mejorLanzamiento.getPorcentaje_beneficio()) + "% (" +	
								df.format(mejorLanzamiento.getBeneficio_bruto()) + " ARS) - " + 
								mejorLanzamiento.getOpcion().getBase() +
								" - prec.compra: " + mejorLanzamiento.getOpcion().getPrecio_compra() + 
								" - cant. compra: " + mejorLanzamiento.getOpcion().getCant_compra());		
		}
	}
}
