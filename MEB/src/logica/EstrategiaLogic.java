package logica;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import pojos.Derivado;
import pojos.Opcion;
import pojos.Papel;

public class EstrategiaLogic {

	static DecimalFormat df = new DecimalFormat("0.00");
	
	public static void generarEstrategia(List<Opcion> listaCalls, List<Opcion> listaPuts, Papel papel, String mes) {
		
		double porcentaje = 0;

		List<Derivado> listaDerivados_C = new ArrayList<Derivado>();
		List<Derivado> listaDerivados_P = new ArrayList<Derivado>();
		
		for (Opcion opcion : listaCalls) {
			if (mes == null || opcion.getBase().contains(mes)){
				porcentaje = opcion.getPrecio_venta()*100/papel.getPrecio_papel();
				Derivado vDerivado = new Derivado(opcion, papel, opcion.getPrecio_venta(),	porcentaje);
				listaDerivados_C.add(vDerivado);
			}
		}
		
		for (Opcion opcion : listaPuts) {
			if (mes == null || opcion.getBase().contains(mes)){
				porcentaje = opcion.getPrecio_venta()*100/papel.getPrecio_papel();
				Derivado vDerivado = new Derivado(opcion, papel, opcion.getPrecio_venta(),	porcentaje);
				listaDerivados_P.add(vDerivado);
			}
		}
		
		EstrategiaLogic.ordenarPorcentajeBeneficio(listaDerivados_C);
		EstrategiaLogic.ordenarPorcentajeBeneficio(listaDerivados_P);
		
		mostrarListado(listaDerivados_C,listaDerivados_P);			
				
	}
	
	private static void mostrarListado(List<Derivado> listaDerivados_C, List<Derivado> listaDerivados_P) {

		System.out.println("Estrategia - Calls - " + listaDerivados_C.get(0).getPapel().getSimbolo_papel() + ": " + listaDerivados_C.get(0).getPapel().getPrecio_papel());
		
		for(Derivado vDerivado : listaDerivados_C){

			if(vDerivado.getOpcion().getCant_venta() == 0) continue;
			
			if(vDerivado.getOpcion().getStrike() < vDerivado.getPapel().getPrecio_papel()){
				System.out.println(
					   "Cobertura: " +	
					   df.format(vDerivado.getPorcentaje_beneficio()) + "% " +
			           "(benef.bruto: " + df.format(vDerivado.getBeneficio_bruto()) + ") - " +
			           vDerivado.getOpcion().getBase() + ": " +
			           vDerivado.getOpcion().getPrecio_venta() +  
			           " - cantidad: " + vDerivado.getOpcion().getCant_venta());
			}else{
				System.out.println(
					   "Variacion: " +	
					   df.format(vDerivado.getPorcentaje_beneficio()) + "% " +
			           "(benef.bruto: " + df.format(vDerivado.getBeneficio_bruto()) + ") - " +
			           vDerivado.getOpcion().getBase() + ": " +
			           vDerivado.getOpcion().getPrecio_venta() +  
			           " - cantidad: " + vDerivado.getOpcion().getCant_venta());
			}
		}
		
		System.out.println("");
		System.out.println("Estrategia - Puts - " + listaDerivados_C.get(0).getPapel().getSimbolo_papel() + ": " + listaDerivados_C.get(0).getPapel().getPrecio_papel());
		
		for(Derivado vDerivado : listaDerivados_P){
			
			if(vDerivado.getOpcion().getCant_venta() == 0) continue;

			if(vDerivado.getOpcion().getStrike() > vDerivado.getPapel().getPrecio_papel()){
				System.out.println(
					   "Cobertura: " +	
					   df.format(vDerivado.getPorcentaje_beneficio()) + "% " +
			           "(benef.bruto: " + df.format(vDerivado.getBeneficio_bruto()) + ") - " +
			           vDerivado.getOpcion().getBase() + ": " +
			           vDerivado.getOpcion().getPrecio_venta() +  
			           " - cantidad: " + vDerivado.getOpcion().getCant_venta());
			}else{
				System.out.println(
					   "Variacion: " +	
					   df.format(vDerivado.getPorcentaje_beneficio()) + "% " +
			           "(benef.bruto: " + df.format(vDerivado.getBeneficio_bruto()) + ") - " +
			           vDerivado.getOpcion().getBase() + ": " +
			           vDerivado.getOpcion().getPrecio_venta() +  
			           " - cantidad: " + vDerivado.getOpcion().getCant_venta());
			}
		}
	}
		
	/**
	 * Ordenamiento por precio
	 */
	public static void ordenarPorcentajeBeneficio(List<Derivado> listaDerivados) {
		
		Collections.sort(listaDerivados, new Comparator<Derivado>(){

			@Override
			public int compare(Derivado mejor1, Derivado mejor2) {
				//System.out.println("tipo:" + mejor1.getOpcion().getTipo());	
				return new Double(mejor2.getPorcentaje_beneficio()).compareTo(new Double(mejor1.getPorcentaje_beneficio()));
			}
			
		});
		
	}

}
