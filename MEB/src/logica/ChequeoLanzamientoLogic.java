package logica;

import java.util.ArrayList;
import java.util.List;
import pojos.Opcion;
import pojos.OpcionLanzada;
import pojos.Papel;

public class ChequeoLanzamientoLogic {
	
	public void chequearEstados(Papel papel) {
		
		if (papel.getPrecio_papel() > papel.getPrecio_resistencia()){
			
			System.out.println("Estado: " + papel.getSimbolo_papel() + ": " + 
					papel.getPrecio_papel() + " rompió resistencia: " + papel.getPrecio_resistencia());
		}
		
		if (papel.getPrecio_papel() < papel.getPrecio_soporte()){
			
			System.out.println("Estado: " + papel.getSimbolo_papel() + ": " + 
					papel.getPrecio_papel() + " rompió soporte: " + papel.getPrecio_soporte());
		}
		
		if (papel.getPrecio_papel() < papel.getPrecio_resistencia()
				&& papel.getPrecio_papel() > papel.getPrecio_soporte()){
			
			System.out.println("Estado: " + papel.getSimbolo_papel() + ": " + 
					papel.getPrecio_papel() + " - No rompió Viejos Rangos: [" + papel.getPrecio_resistencia() + 
					" - "  + papel.getPrecio_soporte() + "]");
		}
		
	}

	public ArrayList<Opcion> seleccionarBasesMes(List<Opcion> listaOpciones, OpcionLanzada opcionLanzada) {
		
		ArrayList<Opcion> listadoOpciones = (ArrayList<Opcion>) listaOpciones;
		ArrayList<Opcion> listadoAuxOpciones = new ArrayList<Opcion> ();
		
		String mesLanzamiento = opcionLanzada.getMes_lanzamiento(); 
		//System.out.println("mesLanzamiento: " + mesLanzamiento);
		
		for (Opcion option : listadoOpciones) {
			
			if (option.getBase().contains(mesLanzamiento)){
				
				listadoAuxOpciones.add(option);
				//System.out.println("Base: " + option.getBase().toString());
			}			
		}
		return listadoAuxOpciones;
		
	}

	public Opcion seleccionarStrikeMenor(ArrayList<Opcion> opcionesSeleccionadas, Papel papel) {
		
		Opcion opcionMenorStrike = null;
		double strike = 0;
		
		for (Opcion option : opcionesSeleccionadas) {
			
			if (option.getStrike() < papel.getPrecio_papel()){
						
				if (option.getStrike() > strike){
					
					opcionMenorStrike = option;
					strike = opcionMenorStrike.getStrike();
				}	
			}			
		}
		//System.out.println("opc. cercana de menor strike: " + opcionMenorStrike.getBase());
		return opcionMenorStrike;
	}
	
	
	public Opcion seleccionarStrikeMayor(ArrayList<Opcion> opcionesSeleccionadas, Papel papel) {
		
		Opcion opcionMayorStrike = null;
		double strike = 10000;

		for (Opcion option : opcionesSeleccionadas) {
			
			if (option.getStrike() > papel.getPrecio_papel()){
				
				if (option.getStrike() < strike){
					
					opcionMayorStrike = option;
					strike = opcionMayorStrike.getStrike();
				}	
			}			
		}
		//System.out.println("opc. cercana de mayor strike: " + opcionMayorStrike.getBase());
		return opcionMayorStrike;
	}

	
	public void recalcularRango(Opcion opcionStrikeMenor, Opcion opcionStrikeMayor, boolean isCall) {
		
		double newResistencia = 0;
		double newSoporte = 0; 

		if (opcionStrikeMenor == null && opcionStrikeMayor == null){
			
			System.out.println("No se pudieron calcular rangos");
			return;
		}	
			
		if(isCall){
			
			if (opcionStrikeMenor == null){					
				newResistencia = opcionStrikeMayor.getStrike() + opcionStrikeMayor.getPrecio_venta();
				System.out.println("Recalculando resistencia: " + newResistencia);
				return;
			}
			
			if (opcionStrikeMayor == null){					
				newResistencia = opcionStrikeMenor.getStrike() + opcionStrikeMenor.getPrecio_venta();
				System.out.println("Recalculando resistencia: " + newResistencia);
				return;
			}
			
			newResistencia = (opcionStrikeMenor.getStrike() + opcionStrikeMenor.getPrecio_venta() + 
			          opcionStrikeMayor.getStrike() + opcionStrikeMayor.getPrecio_venta()) / 2;
			
			System.out.println("Recalculando resistencia: " + newResistencia);
				
			
			
		}else{
			
			if (opcionStrikeMenor == null){					
				newSoporte = opcionStrikeMayor.getStrike() - opcionStrikeMayor.getPrecio_venta();
				System.out.println("Recalculando soporte: " + newSoporte);
				return;
			}
			
			if (opcionStrikeMayor == null){					
				newSoporte = opcionStrikeMenor.getStrike() - opcionStrikeMenor.getPrecio_venta();
				System.out.println("Recalculando soporte: " + newSoporte);
				return;
			}	
			
			newSoporte = ((opcionStrikeMenor.getStrike() - opcionStrikeMenor.getPrecio_venta()) + 
			          (opcionStrikeMayor.getStrike() - opcionStrikeMayor.getPrecio_venta())) / 2;
						
			System.out.println("Recalculando soporte: " + newSoporte);
		}

	}
	

	public void recomprarCall(List<Opcion> listaOpciones, OpcionLanzada opcionLanzada) {
		
		for (Opcion option : listaOpciones) {
			
			if (option != null){
				
				if (option.getBase().equals(opcionLanzada.getBase())){
					
					System.out.println("Recompra: " + option.getBase() + " - cant: " + option.getCant_venta() +
							" - prec. vta.: " +option.getPrecio_venta());
				}
				
			}			
		
		}
	}




}
