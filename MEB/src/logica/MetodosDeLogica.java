package logica;

import control.PropertiesController;

public class MetodosDeLogica {

	static String RUTA_CARPETA_PRINCIPAL = null;
	static String MARCA = "BajadasHTML";
	
	/**
	 * 
	 * @return
	 */
	public static boolean getMOSTRAR_PROMEDIOS(){
	
		//default 1 - NO MOSTRAR
		if(PropertiesController.NO_MOSTRAR_PROMEDIOS == null) return false; 
			
		if (PropertiesController.NO_MOSTRAR_PROMEDIOS.trim().equals("1")) return false;
			
		return true;
	}
	
	/**
	 * 
	 * @return
	 */
	public static String getRUTA_CARPETA_OPCIONES(){
	
		if(RUTA_CARPETA_PRINCIPAL != null){
			return RUTA_CARPETA_PRINCIPAL;
		}
		
		//1a- Fuerza recuperacion desde properties
		if (PropertiesController.RUTA_CARPETA_OPCIONES != null){
			System.out.println("Usando RUTA_CARPETA_OPCIONES del properties");
			RUTA_CARPETA_PRINCIPAL = PropertiesController.RUTA_CARPETA_OPCIONES;
			return RUTA_CARPETA_PRINCIPAL;
		}
		
		//1b- Recuperacion ruta principal del sistema
		String carpetaRaiz = System.getProperty("user.dir");
		System.out.println("user.dir:" + carpetaRaiz);
		
		if(carpetaRaiz.contains(MARCA)){
			//ejecucion como jar en linux
			int max = carpetaRaiz.indexOf(MARCA) + MARCA.length();
			RUTA_CARPETA_PRINCIPAL = carpetaRaiz.substring(0, max+1);
			System.out.println("RUTA_CARPETA_PRINCIPAL:" + RUTA_CARPETA_PRINCIPAL);
			return RUTA_CARPETA_PRINCIPAL;
			
		} else {
			
			//1c- Ejecutando en IDE ->  /home/julio/WS_Web_2016/repo_junan02/MEB
			int desde = "/home/".length() + 1;
			int hasta = carpetaRaiz.indexOf("/", desde);
			
			if(hasta > 0){
				RUTA_CARPETA_PRINCIPAL = carpetaRaiz.substring(0,hasta + 1) + MARCA + "/";
				System.out.println("Ejecucion en IDE:" + RUTA_CARPETA_PRINCIPAL);
			}
		}
	
		return RUTA_CARPETA_PRINCIPAL;
	}
	
}
