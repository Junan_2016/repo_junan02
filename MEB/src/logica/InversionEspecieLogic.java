package logica;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import control.PropertiesController;
import pojos.Especie;
import pojos.InversionEspecie;
import util.Validacion;


public class InversionEspecieLogic {
	
	static DecimalFormat df = new DecimalFormat("0.00");
	static SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
	static List<InversionEspecie> listaInversionesPromedio = new ArrayList<InversionEspecie> ();
	
	/**
	 * 
	 * @param invActual 
	 * @return
	 */
	public static List<InversionEspecie> getInversionesEspecie(String invActual){

		List<InversionEspecie> listaInversiones = new ArrayList<InversionEspecie> ();
		List<InversionEspecie> listaInvPromAux = new ArrayList<InversionEspecie> ();
		InversionEspecie inversionEspecie = null;
		InversionEspecie inversionAux = null;
		
		try {
	
			//APBR:300:21:6400:2015_12_15-GGAL:200:43:8679.55:2015_11_15
			String[] inversionActualProperties = invActual.split("-");

			for(String inversion: inversionActualProperties){
				
				String[] inversionActual = inversion.split(":");
				inversionEspecie = new InversionEspecie();
				
				inversionEspecie.setSimbolo_especie(inversionActual[0]);
				inversionEspecie.setCantidad_compra_especie(Integer.parseInt(inversionActual[1]));
				inversionEspecie.setPrecio_compra_especie(Double.parseDouble(inversionActual[2]));
				
				if(inversionActual.length == 4 || inversionActual.length == 5){
					inversionEspecie.setTotal_compra_y_comision(Double.parseDouble(inversionActual[3]));
				}					
				
				if(inversionActual.length == 5){
					
					if (inversionActual[4] != null && !inversionActual[4].equalsIgnoreCase("") && !inversionActual[4].equals("0")){
						
						String[] fechaCompra = inversionActual[4].split("_");					
						int anno = Integer.parseInt(fechaCompra[0]);
						int mes = Integer.parseInt(fechaCompra[1]);
						int dia = Integer.parseInt(fechaCompra[2]);
						
						Calendar fecha = new GregorianCalendar(anno, mes-1, dia);						
						inversionEspecie.setFecha_compra_especie(fecha);
					}
				}		
				
				listaInversiones.add(inversionEspecie);
				inversionAux = (InversionEspecie) inversionEspecie.clone();
				listaInvPromAux.add(inversionAux);
			}
			
			//arma listaInversionesPromedio	- 1er parte	
			for (InversionEspecie invEspecieProm : listaInvPromAux) {	
				
				if (listaInversionesPromedio != null && listaInversionesPromedio.size() > 0) {
					
					for (int i = 0; i < listaInversionesPromedio.size(); i++) {
						
						InversionEspecie vInvEspecieProm = listaInversionesPromedio.get(i);
						
						if (invEspecieProm.getSimbolo_especie().equalsIgnoreCase(vInvEspecieProm.getSimbolo_especie()) ) {
							
							invEspecieProm.setCantidad_compra_especie(invEspecieProm.getCantidad_compra_especie() + vInvEspecieProm.getCantidad_compra_especie());
							invEspecieProm.setTotal_compra_y_comision(invEspecieProm.getTotal_compra_y_comision() + vInvEspecieProm.getTotal_compra_y_comision());
							invEspecieProm.setPrecio_compra_especie(vInvEspecieProm.getTotal_compra_y_comision() / vInvEspecieProm.getCantidad_compra_especie());
							
							i = -1;
							listaInversionesPromedio.remove(vInvEspecieProm);
						}
					}
					
					listaInversionesPromedio.add(invEspecieProm);
				} 
				else 
					listaInversionesPromedio.add(invEspecieProm);
			}
						
			/*
			for (InversionEspecie vInvEspecieProm : listaInversionesPromedio) {	
				System.out.println(vInvEspecieProm.toString());
			}
			*/
			return listaInversiones;

		} catch (Exception e) {
			
			System.err.println("Error Properties INV_ACTUAL es nula");
			//e.printStackTrace();
			return null;
		}

	}
	

	/**
	 * 
	 * @param listaInversionEspecie
	 * @param listaEspecies
	 */
	public static void verPanelDeControlInversiones(List<InversionEspecie> listaInversionEspecie, List<Especie> listaEspecies){
		
		double totalOriginal = 0;
		double totalGanancias = 0;
		double resultado = 0;
		
		System.out.println("-----------------------------------------------------------------");

		for (InversionEspecie vInversionEspecie : listaInversionEspecie) {	
			
			String simbolo = vInversionEspecie.getSimbolo_especie();
			int cantidadCompra = vInversionEspecie.getCantidad_compra_especie();
			Double precioCompra = vInversionEspecie.getPrecio_compra_especie();
			Double totalComprado = cantidadCompra * precioCompra; 
			
			Double totalMasComision = vInversionEspecie.getTotal_compra_y_comision(); 
			Double comision = new Double(0);
			
			if(totalMasComision != null && totalMasComision != 0){
				comision = totalMasComision - totalComprado;
			}
			else{
				totalMasComision = totalComprado;
			}
			
			Double porcentajeComision = new Double(0);
			if(totalComprado != null && totalComprado != 0)				
				porcentajeComision = (comision*100)/totalComprado;	
			
			totalOriginal = totalOriginal + totalMasComision;
			
			System.out.println(simbolo + " (" + cantidadCompra + " a " + df.format(precioCompra) + " = " + df.format(totalComprado) + ")" +
				" Comi:" + df.format(comision) + "(" + df.format(porcentajeComision) + "%) - Tot:" + df.format(totalMasComision));
			
			for(Especie vEspecie : listaEspecies){
				
				Double precioActual = null;
				Double totalActual = null;
				Double gananciaBruta = null;
				Double porcentajeGananciaBruta = new Double(0);
				Double gananciaNeta = null; 
				Double porcentajeGanancia = new Double(0);
				
				if(vEspecie.getSimbolo_especie().equalsIgnoreCase(vInversionEspecie.getSimbolo_especie())){
					
					precioActual = vEspecie.getPrecio_especie();
					totalActual = precioActual * cantidadCompra; 
					
					gananciaBruta = totalActual - totalComprado; //sin tener en cuenta las comisiones
										
					if(totalComprado != null && totalComprado != 0)
						porcentajeGananciaBruta = (gananciaBruta*100)/totalComprado;
					
					gananciaNeta = totalActual - totalMasComision; 
					
					if(totalMasComision != null && totalMasComision != 0){
						gananciaNeta = totalActual - totalMasComision;
						porcentajeGanancia = (gananciaNeta*100)/totalMasComision;
					}
					else {
						gananciaNeta = gananciaBruta;
						porcentajeGanancia = porcentajeGananciaBruta;
					}	
					
					if (vInversionEspecie.getFecha_compra_especie() != null) {
						
						double cantMeses = Validacion.calcularMesesDecimal(vInversionEspecie.getFecha_compra_especie());
						System.out.println(df.format(cantMeses) + " meses" +
								   " (" + sdf.format(vInversionEspecie.getFecha_compra_especie().getTime()) +
								   ") >> [" + df.format(porcentajeGanancia/cantMeses) + "% x mes]");
					}
					
					totalGanancias = totalGanancias + gananciaNeta;

					System.out.println(
							"Máx:" + df.format(vEspecie.getPrecio_maximo()) +  
							"- Min:" + df.format(vEspecie.getPrecio_minimo()) + 
							"- " + vEspecie.getFibos());

					System.out.println("[Prec:" + df.format(precioActual) +	"]" + 
							"- PrecAnt:" + df.format(vEspecie.getPrecio_anterior()) +
							"- Brut:" + df.format(gananciaBruta) + "(" + df.format(porcentajeGananciaBruta) + "%)" +
							"- Gan:" + df.format(gananciaNeta) + "[" + df.format(porcentajeGanancia) + "%]" );
					
					//arma listaInversionesPromedio	- 2da parte				
					if (listaInversionesPromedio != null && listaInversionesPromedio.size() > 0) { 
						
						for (InversionEspecie vInvEspecieProm : listaInversionesPromedio) {	
							
							if (vInversionEspecie.getSimbolo_especie().equalsIgnoreCase(vInvEspecieProm.getSimbolo_especie()) ) {
								
								vInvEspecieProm.setPrecio_actual(precioActual);
								vInvEspecieProm.setPrecio_maximo(vEspecie.getPrecio_maximo());
								
								if (vInvEspecieProm.getGanancia_bruta() == 0) {
									
									  vInvEspecieProm.setGanancia_bruta(gananciaBruta);
									  vInvEspecieProm.setPorcentaje_ganancia_bruta(porcentajeGananciaBruta);
									  vInvEspecieProm.setGanancia_neta(gananciaNeta);
									  vInvEspecieProm.setPorcentaje_ganancia(porcentajeGanancia);
									  vInvEspecieProm.setComision(comision);
									  vInvEspecieProm.setPorcentaje_comision(porcentajeComision);
								} 								
								else {
									  vInvEspecieProm.setGanancia_bruta((vInvEspecieProm.getGanancia_bruta() + gananciaBruta) / 2);
									  vInvEspecieProm.setPorcentaje_ganancia_bruta((vInvEspecieProm.getPorcentaje_ganancia_bruta() + porcentajeGananciaBruta) / 2);
									  vInvEspecieProm.setGanancia_neta((vInvEspecieProm.getGanancia_neta() + gananciaNeta) / 2);
									  vInvEspecieProm.setPorcentaje_ganancia((vInvEspecieProm.getPorcentaje_ganancia() + porcentajeGanancia) / 2);
									  vInvEspecieProm.setComision((vInvEspecieProm.getComision() + comision) / 2);
									  vInvEspecieProm.setPorcentaje_comision((vInvEspecieProm.getPorcentaje_comision() + porcentajeComision) / 2);
								}
							}
						}
					}
				}
			}

			System.out.println("-----------------------------------------------------------------");
		}
		
		
		if (MetodosDeLogica.getMOSTRAR_PROMEDIOS() && listaInversionesPromedio != null && listaInversionesPromedio.size() > 0) { 
			
			System.out.println("PROMEDIOS:");
			
			for (InversionEspecie invEspecieProm : listaInversionesPromedio) {	
				System.out.println("********************************************************************");
				//System.out.println(vInvEspecieProm.toString2());
				
				System.out.println(invEspecieProm.getSimbolo_especie() + " (" + invEspecieProm.getCantidad_compra_especie() + " a " + 
						df.format(invEspecieProm.getPrecio_compra_especie()) + " = " + df.format(invEspecieProm.getCantidad_compra_especie() * invEspecieProm.getPrecio_compra_especie()) + ")" +
						" Comi:" + df.format(invEspecieProm.getComision()) + "(" + df.format(invEspecieProm.getPorcentaje_comision()) + "%) - Tot:" + df.format(invEspecieProm.getTotal_compra_y_comision()));
				
				System.out.println("[Prec:" + df.format(invEspecieProm.getPrecio_actual()) + 
						"] Máx:" + df.format(invEspecieProm.getPrecio_maximo()) + "- Brut:" + df.format(invEspecieProm.getGanancia_bruta()) + "(" + df.format(invEspecieProm.getPorcentaje_ganancia_bruta()) + "%)" +
						"- Gan:" + df.format(invEspecieProm.getGanancia_neta()) + "[" + df.format(invEspecieProm.getPorcentaje_ganancia_bruta()) + "%]" );
				
			}
			System.out.println("********************************************************************");
			
			listaInversionesPromedio.clear();
		}
		resultado = totalOriginal + totalGanancias;
		
		System.out.println("Total Original: " + df.format(totalOriginal) + "-  Ganancias: " + df.format(totalGanancias));
		System.out.println("Resultado: " + df.format(resultado) + "[" + df.format(((resultado * 100) / totalOriginal) - 100) + "%]");
		System.out.println("-----------------------------------------------------------------");
	}
	
	
	

}
