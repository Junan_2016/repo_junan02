package logica;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import pojos.Derivado;
import pojos.Opcion;
import pojos.Papel;

public class CYP_Logic{

	static DecimalFormat df = new DecimalFormat("0.00");
	
	public static void generar_CYP(List<Opcion> listaCalls, List<Opcion> listaPuts, Papel papel, String mes) {
		
		double porcentaje = 0;

		List<Derivado> listaDerivados_C = new ArrayList<Derivado>();
		List<Derivado> listaDerivados_P = new ArrayList<Derivado>();
		
		for (Opcion opcion : listaCalls) {
			if (mes == null || opcion.getBase().contains(mes)){
				porcentaje = opcion.getPrecio_venta()*100/papel.getPrecio_papel();
				Derivado vDerivado = new Derivado(opcion, papel, opcion.getPrecio_venta(),	porcentaje);
				listaDerivados_C.add(vDerivado);
			}
		}
		
		for (Opcion opcion : listaPuts) {
			if (mes == null || opcion.getBase().contains(mes)){
				porcentaje = opcion.getPrecio_venta()*100/papel.getPrecio_papel();
				Derivado vDerivado = new Derivado(opcion, papel, opcion.getPrecio_venta(),	porcentaje);
				listaDerivados_P.add(vDerivado);
			}
		}
		
		CYP_Logic.ordenarPorcentajeBeneficio(listaDerivados_C);
		CYP_Logic.ordenarPorcentajeBeneficio(listaDerivados_P);
		
		mostrarListado(listaDerivados_C,listaDerivados_P);			
				
	}
	
	private static void mostrarListado(List<Derivado> listaDerivados_C, List<Derivado> listaDerivados_P) {
		System.out.println("");
		System.out.println("CYP - C_ - " + listaDerivados_C.get(0).getPapel().getSimbolo_papel() + ": " + listaDerivados_C.get(0).getPapel().getPrecio_papel());
		String cartel = null;
				
		for(Derivado vDerivado : listaDerivados_C){

			if(vDerivado.getOpcion().getCant_venta() == 0) continue;
			
			cartel = df.format(vDerivado.getPorcentaje_beneficio()) + "% " +
			           "(benef.bruto: " + df.format(vDerivado.getBeneficio_bruto()) + ") - " +
			           vDerivado.getOpcion().getBase() + ": " +
			           vDerivado.getOpcion().getPrecio_venta() +  
			           " - cant: " + vDerivado.getOpcion().getCant_venta() +
			           " - monto: " +  df.format(vDerivado.getOpcion().getCant_venta()*100* vDerivado.getOpcion().getPrecio_venta())+
			           " - vol: " + vDerivado.getOpcion().getVolumen() +
			           " - mkt: " + vDerivado.getOpcion().getPorcMKT();
			
			if(vDerivado.getOpcion().getStrike() < vDerivado.getPapel().getPrecio_papel()){
				cartel =  "Cobertura: " + cartel;
			}else{
				cartel =  "Variacion: " + cartel;
			}
			System.out.println(cartel);
		}
		
		System.out.println("");
		System.out.println("CYP - P_ - " + listaDerivados_C.get(0).getPapel().getSimbolo_papel() + ": " + listaDerivados_C.get(0).getPapel().getPrecio_papel());
		
		for(Derivado vDerivado : listaDerivados_P){
			
			if(vDerivado.getOpcion().getCant_venta() == 0) continue;

			cartel =  df.format(vDerivado.getPorcentaje_beneficio()) + "% " +
			           "(benef.bruto: " + df.format(vDerivado.getBeneficio_bruto()) + ") - " +
			           vDerivado.getOpcion().getBase() + ": " +
			           vDerivado.getOpcion().getPrecio_venta() +  
			           " - cant: " + vDerivado.getOpcion().getCant_venta() +
			           " - monto: " + df.format(vDerivado.getOpcion().getCant_venta() *100* vDerivado.getOpcion().getPrecio_venta())+
			           " - vol: " + vDerivado.getOpcion().getVolumen() +
			           " - mkt: " + vDerivado.getOpcion().getPorcMKT();
			
			if(vDerivado.getOpcion().getStrike() > vDerivado.getPapel().getPrecio_papel()){
				cartel =  "Cobertura: " + cartel;
			}else{
				cartel =  "Variacion: " + cartel;
			}
			System.out.println(cartel);
		}
	}
	
	
	/**
	 * Ordenamiento por precio
	 */
	public static void ordenarPorcentajeBeneficio(List<Derivado> listaDerivados) {
		
		Collections.sort(listaDerivados, new Comparator<Derivado>(){

			@Override
			public int compare(Derivado mejor1, Derivado mejor2) {
				//System.out.println("tipo:" + mejor1.getOpcion().getTipo());	
				return new Double(mejor2.getPorcentaje_beneficio()).compareTo(new Double(mejor1.getPorcentaje_beneficio()));
			}
			
		});
		
	}

}
