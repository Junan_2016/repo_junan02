package logica;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import control.BajadaController;
import pojos.Opcion;
import util.Constantes;

public class BajadaLogic {
		
	/**
	 * Procesa HTML desde file
	 * @param url
	 * @return
	 */
	public static List<Opcion> procesaHTML_from_file(final String rutaArchivo, final String tipoOpcion){
		Document doc = null;
		File input = null;
		
		try {
			input = new File(rutaArchivo);
			doc = Jsoup.parse(input,null);
		} catch (IOException e) {
			System.err.println("Error al procesar el archivo: " + rutaArchivo);
			e.printStackTrace();
			return null;
		}
		
		try{
			List<Opcion> listaOpciones = parseo_HTML(doc,tipoOpcion,rutaArchivo);
			return listaOpciones;
		
		} catch (Exception e) {
			System.err.println("Error al parsear el archivo: " + rutaArchivo);
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Metodo de parseo de HTML
	 * @param doc
	 * @return
	 */
	private static List<Opcion> parseo_HTML(Document doc, final String tipoOpcion,String rutaArchivo)throws Exception {
		List<Opcion> listaOpciones = new ArrayList<Opcion>();
		
		Element tablaOpciones = null;
		if(tipoOpcion.equalsIgnoreCase(Constantes.CALL_CHAR)) tablaOpciones = doc.getElementById("tCallsListado");
		if(tipoOpcion.equalsIgnoreCase(Constantes.PUT_CHAR)) tablaOpciones = doc.getElementById("tPutsListado");
				
		Elements listaTR = tablaOpciones.getElementsByTag("tr");
		for (Element tr : listaTR) {
			Elements listaTD = tr.getElementsByTag("td");

			Opcion vOpcion = new Opcion();
			
			vOpcion.setTipo(tipoOpcion.charAt(0));
			if (!listaTD.get(0).text().equalsIgnoreCase("")) vOpcion.setBase(listaTD.get(0).text());
			if (!listaTD.get(1).text().equalsIgnoreCase("")) vOpcion.setCant_compra(Integer.valueOf(listaTD.get(1).text()));
			if (!listaTD.get(2).text().equalsIgnoreCase("")) vOpcion.setPrecio_compra(Double.valueOf(listaTD.get(2).text().replace(",",".")));
			if (!listaTD.get(3).text().equalsIgnoreCase("")) vOpcion.setPrecio_venta(Double.valueOf(listaTD.get(3).text().replace(",",".")));
			if (!listaTD.get(4).text().equalsIgnoreCase("")) vOpcion.setCant_venta(Integer.valueOf(listaTD.get(4).text()));

			if (!listaTD.get(6).text().equalsIgnoreCase("")) vOpcion.setPorcMKT(listaTD.get(6).text().replace(",","."));
			if (!listaTD.get(11).text().equalsIgnoreCase("")) vOpcion.setVolumen(listaTD.get(11).text().replace(",","."));
			
			vOpcion.setStrike(recuperaStrike(vOpcion.getBase(),rutaArchivo));
			
			listaOpciones.add(vOpcion);
		}
		return listaOpciones;
	}
		
	/**
	 * GFGV10923J
	 * @param base
	 * @return
	 */
	private static double recuperaStrike(String base, String rutaArchivo) {
		StringBuilder sb = new StringBuilder();
		
		int longitud = base.length();
		for (int j=0; j<longitud; j++){
			String aux = base.substring(j,j+1);
			if (aux.matches("[0-9/.]*"))sb.append(aux);
		}
		String valor = sb.toString();

		if(valor.contains(".")){
			//No hay excepciones en dicho strike
			if(valor.startsWith(".")) valor = valor.substring(1);
			if(valor.endsWith("."))valor = valor.substring(0,valor.length()-1);
		} else {

			//manejo de excepcion en formato del strike
			Map mapaStrikeExcepciones = BajadaController.getMapaStrikeExcepciones();
			Set <String> coleccionmPapeles = mapaStrikeExcepciones.keySet();
					
			for(String key : coleccionmPapeles){
				if (rutaArchivo.contains(key)){
					int cantidadEnteros = Integer.parseInt(((String)mapaStrikeExcepciones.get(key)));
					
					String parteEntera = valor.substring(0,cantidadEnteros);
					String partedecimal = valor.substring(cantidadEnteros);
					//System.out.println(parteEntera + "." + partedecimal);
					return Double.parseDouble(parteEntera + "." + partedecimal);
				}
				
			}
		}
		
		return Double.parseDouble(valor.trim());
	}
}
