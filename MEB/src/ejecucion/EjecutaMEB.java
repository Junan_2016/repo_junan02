package ejecucion;

import logica.MetodosDeLogica;
import control.BajadaRavaController;
import control.CYPController;
import control.EstrategiaController;
import control.MejorGananciaController;
import control.MejorLanzamientoController;
import control.MejorProteccionController;
import control.RangosController;
import control.SeguridadController;

public class EjecutaMEB {

	public static void main(String[] args) {
		
		if (SeguridadController.comparaHash(SeguridadController.getHash())) {

				if (args.length == 0) {
					System.err.println("Parametro incorrecto");
					return;
				}
						
				try {
					Thread.sleep(2000);
					
					//1(RAVA)-2(CYP)-3(GANANCIA)-4(PROTECCION)-5(LANZAMIENTO)-6(RANGO)-7(ESTRATEGIA)

					//para cargar ruta
				    MetodosDeLogica.getRUTA_CARPETA_OPCIONES();
					
					if (args[0].equals("1")) {
						System.out.println("Ejecucion RV");
						BajadaRavaController.obtenerPanelDeControl();
					};

					
					if (args[0].equals("2")) {
						System.out.println("Ejecucion CYP");
						CYPController.obtener_CYP();
					};
										
					if (args[0].equals("3")) {
						System.out.println("Ejecucion GAN");
						MejorGananciaController.mostrarMejorCall();
					}
					
					if (args[0].equals("4")) {
						System.out.println("Ejecucion PROT");
						MejorProteccionController.mostrarMejorPut();
					}
					 
					if (args[0].equals("5")) {
						System.out.println("Ejecucion LANZ");
						MejorLanzamientoController.mostrarMejorLanzamiento();
					}
					
					if (args[0].equals("6")) {
						System.out.println("Ejecucion RANGO");
						RangosController.chequearRangos();
					}
				
					if (args[0].equals("7"))  {
						System.out.println("Ejecucion ESTRAT");
						EstrategiaController.mostrarMejorEstrategia();
					}
					
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
		} else {
			System.err.println("Texto incorrecto");
		}
	}

}
