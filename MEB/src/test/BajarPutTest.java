package test;

import static org.junit.Assert.*;

import java.util.List;

import mock.PutMock;

import org.junit.BeforeClass;
import org.junit.Test;

import pojos.Opcion;

public class BajarPutTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void test() {
		
		final List <Opcion> lista = PutMock.crearListaPuts();
		
		assertEquals(true, lista.size()>0);
		
	}

}
