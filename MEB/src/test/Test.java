package test;

public class Test {

	public static void main(String[] args) {
		String base= "TS.13.0.AB";
		System.out.println("Strike: " + recuperaStrike(base));
		
	}

	/**
	 * @param base
	 */
	private static double recuperaStrike(String base) {
		StringBuilder sb = new StringBuilder();
		
		int longitud = base.length();
		for (int j=0; j<longitud; j++){
			String aux = base.substring(j,j+1);
			if (aux.matches("[0-9/.]*"))sb.append(aux);
		}

		String valor = sb.toString();
		if(valor.startsWith(".")) valor = valor.substring(1);
		if(valor.endsWith("."))valor = valor.substring(0,valor.length()-1);
				
		return Double.parseDouble(valor.trim());
		
	}

}
