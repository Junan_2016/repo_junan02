package util;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validacion {
	
    static final String IDENTIF_PATTERN = "[0-9]{1,3}([\\.][0-9]{3})?([\\,][0-9]{3})?$";
      
    /**
     * 
     * @param identificador
     * @return
     */
    public static boolean esIdentificadorValido(String identificador){
       	
    	if(identificador == null || "".equals(identificador) || "-".equals(identificador)) return false;
    	
    	identificador = identificador.trim();
    	final Pattern pattern = Pattern.compile(IDENTIF_PATTERN);
    	final Matcher matcher = pattern.matcher(identificador);
    	return matcher.matches();
    	
    }   
    
    /**
     * 
     * @param fechaInicial
     * @return
     */
    public static long calcularMeses(Calendar fechaInicial){
       	
    	if(fechaInicial == null) return 0;
    	
        Calendar calFechaActual = Calendar.getInstance();    	
    	    	
    	final long resto = calFechaActual.getTimeInMillis() - fechaInicial.getTimeInMillis();
    	final long minutes = resto / 1000 / 60;
    	final long hours = minutes / 60;
    	final long days = hours / 24;
    	final long months = days / 30;
    	return months;
    }
    
    /**
     * 
     * @param fechaInicial
     * @return
     */
    public static double calcularMesesDecimal(Calendar fechaInicial){
       	
    	if(fechaInicial == null) return 0;
    	
        Calendar calFechaActual = Calendar.getInstance();    	
    	    	
    	final double resto = calFechaActual.getTimeInMillis() - fechaInicial.getTimeInMillis();
    	final double minutes = resto / 1000 / 60;
    	final double hours = minutes / 60;
    	final double days = hours / 24;
    	final double months = days / 30;
    	return months;
    }

	
}