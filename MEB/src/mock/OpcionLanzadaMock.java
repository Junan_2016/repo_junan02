package mock;

import pojos.Opcion;
import pojos.OpcionLanzada;

public class OpcionLanzadaMock extends Opcion{
	
	public static OpcionLanzada crearOpcionLanzadaMock() {
		
		OpcionLanzada opcionLanzada = new OpcionLanzada();
		
		opcionLanzada.setBase("GFGC23.0AB");
		
		opcionLanzada.setMes_lanzamiento("AB");
	
		
		return opcionLanzada;
		
	}

}
