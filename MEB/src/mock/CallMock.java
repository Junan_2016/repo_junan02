package mock;

import java.util.ArrayList;

import pojos.Opcion;

public class CallMock {
	
	
	public static ArrayList<Opcion> crearListaCalls() {
		
		ArrayList<Opcion> listaCallsMock = new ArrayList<Opcion> ();
		
		Opcion call1 = null;
		Opcion call2 = null;
		Opcion call3 = null;
		Opcion call4 = null;
		Opcion call5 = null;
		Opcion call6 = null;
		
		call1 = new Opcion ();
		call1.setBase("GFGC23.0AB");
		call1.setCant_compra(60);
		call1.setCant_venta(100);
		call1.setPrecio_compra(3.35);
		call1.setPrecio_venta(3.59);
		call1.setStrike(23);
		call1.setTipo('C');
		
		call2 = new Opcion ();
		call2.setBase("GFGC24.0AB ");
		call2.setCant_compra(23);
		call2.setCant_venta(110);
		call2.setPrecio_compra(2.74);
		call2.setPrecio_venta(2.85);
		call2.setStrike(24);
		call2.setTipo('C');
		
		call3 = new Opcion ();
		call3.setBase("GFGC25.0AB");
		call3.setCant_compra(30);
		call3.setCant_venta(336);
		call3.setPrecio_compra(2.16);
		call3.setPrecio_venta(2.24);
		call3.setStrike(25);
		call3.setTipo('C');
		
		call4 = new Opcion ();
		call4.setBase("GFGC26.0AB");
		call4.setCant_compra(40);
		call4.setCant_venta(158);
		call4.setPrecio_compra(1.678);
		call4.setPrecio_venta(1.75);
		call4.setStrike(26);
		call4.setTipo('C');		
		
		call5 = new Opcion ();
		call5.setBase("GFGC25.0JU");
		call5.setCant_compra(30);
		call5.setCant_venta(336);
		call5.setPrecio_compra(2.16);
		call5.setPrecio_venta(2.24);
		call5.setStrike(25);
		call5.setTipo('C');
		
		call6 = new Opcion ();
		call6.setBase("GFGC26.0JU");
		call6.setCant_compra(40);
		call6.setCant_venta(158);
		call6.setPrecio_compra(1.678);
		call6.setPrecio_venta(1.75);
		call6.setStrike(26);
		call6.setTipo('C');		
		
		
		listaCallsMock.add(call1);
		listaCallsMock.add(call2);
		listaCallsMock.add(call3);
		listaCallsMock.add(call4);
		listaCallsMock.add(call5);
		listaCallsMock.add(call6);
		
		return listaCallsMock;		
	
	}

}
