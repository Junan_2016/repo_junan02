package mock;

import java.util.ArrayList;

import pojos.Opcion;

public class PutMock {
	
	
	public static ArrayList<Opcion> crearListaPuts() {
		
		ArrayList<Opcion> listaCallsMock = new ArrayList<Opcion> ();
		
		Opcion call1 = null;
		Opcion call2 = null;
		Opcion call3 = null;
		Opcion call4 = null;
		
		call1 = new Opcion ();
		call1.setBase("GFGV23.0AB");
		call1.setCant_compra(60);
		call1.setCant_venta(100);
		call1.setPrecio_compra(3.35);
		call1.setPrecio_venta(3.59);
		call1.setStrike(23);
		call1.setTipo('P');
		
		call2 = new Opcion ();
		call2.setBase("GFGV24.0AB ");
		call2.setCant_compra(23);
		call2.setCant_venta(110);
		call2.setPrecio_compra(2.74);
		call2.setPrecio_venta(2.85);
		call2.setStrike(24);
		call2.setTipo('P');
		
		call3 = new Opcion ();
		call3.setBase("GFGV25.0AB");
		call3.setCant_compra(30);
		call3.setCant_venta(336);
		call3.setPrecio_compra(2.16);
		call3.setPrecio_venta(2.24);
		call3.setStrike(25);
		call3.setTipo('P');
		
		call4 = new Opcion ();
		call4.setBase("GFGV26.0AB");
		call4.setCant_compra(40);
		call4.setCant_venta(158);
		call4.setPrecio_compra(1.678);
		call4.setPrecio_venta(1.75);
		call4.setStrike(26);
		call4.setTipo('P');		
		
		
		listaCallsMock.add(call1);
		listaCallsMock.add(call2);
		listaCallsMock.add(call3);
		listaCallsMock.add(call4);
		
		return listaCallsMock;		
	
	}

}
