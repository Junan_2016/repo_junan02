package mock;

import pojos.Papel;

public class PapelMock {
	
	public static Papel crearPapel() {
		
		Papel papel = new Papel();
		
		papel.setSimbolo_papel("GGAL");
		papel.setPrecio_papel(21); //24.8
		
		papel.setPrecio_resistencia(24.3);
		papel.setPrecio_soporte(20);
		
		return papel;
		
	}

}
