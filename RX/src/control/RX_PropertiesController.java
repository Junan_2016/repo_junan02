package control;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class RX_PropertiesController {

	static private Properties vProperties; 
	
    static {
    	try {
    		InputStream is = new RX_PropertiesController().getClass().getResourceAsStream("/RX.properties");
    		vProperties = new Properties();
    		vProperties.load(is);
	    } catch (IOException e) {
			System.out.println("Error la cargar el fichero de propiedades: " + e.getMessage());
		}
    }

    public static final String ARCHIVO_CSV_RX = vProperties.getProperty("ARCHIVO_CSV_RX");
 
    public static final String RUTA_CARPETA_BAJADAS_CSV = vProperties.getProperty("RUTA_CARPETA_BAJADAS_CSV");
    
}
