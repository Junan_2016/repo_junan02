package control;

import java.util.List;

import logica.FormatearCSVLogica;
import pojo.CotizacionFormateada;

public class FormatearCSVController {
	
	public static List<CotizacionFormateada> formatearCSV(){
		
		List<CotizacionFormateada> lista = FormatearCSVLogica.formatCSVforRX();
		
		return lista;
	}

}
