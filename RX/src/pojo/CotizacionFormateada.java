package pojo;

import java.text.SimpleDateFormat;

public class CotizacionFormateada {

	String fecha = null;
	double precio_apertura = 0;
	double precio_maximo = 0; 
	double precio_minimo = 0;
	double precio_cierre = 0;
	double volumen = 0;
	String openint = "0";
	
	
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	public double getPrecio_maximo() {
		return precio_maximo;
	}
	public void setPrecio_maximo(double precio_maximo) {
		this.precio_maximo = precio_maximo;
	}
	
	public double getPrecio_minimo() {
		return precio_minimo;
	}
	public void setPrecio_minimo(double precio_minimo) {
		this.precio_minimo = precio_minimo;
	}
	
	public double getPrecio_cierre() {
		return precio_cierre;
	}
	public void setPrecio_cierre(double precio_cierre) {
		this.precio_cierre = precio_cierre;
	}
	
	public double getVolumen() {
		return volumen;
	}
	public void setVolumen(double volumen) {
		this.volumen = volumen;
	}
		
	public double getPrecio_apertura() {
		return precio_apertura;
	}
	public void setPrecio_apertura(double precio_apertura) {
		this.precio_apertura = precio_apertura;
	}
	
	public String getOpenint() {
		return openint;
	}
	public void setOpenint(String openint) {
		this.openint = openint;
	}
	
	
	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd");
		
		result.append("fecha: " + formatter.format(this.fecha) +
		" - precio_apertura: " + this.precio_apertura +
		" - precio_maximo: " + this.precio_maximo +
		" - precio_minimo: " + this.precio_minimo +
		" - precio_cierre: " + this.precio_cierre +
		" - volumen: " + this.volumen);
		
		return result.toString();
	}

}
