package logica;

import java.text.SimpleDateFormat;
import java.util.Date;

import pojo.CotizacionFormateada;

public class CotizacionFormateadaParser {
  
	public static CotizacionFormateada parseEntry(String[] data) {
		
        Date fecha = new Date(Long.parseLong(data[0]));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String fechaFormateada = sdf.format(fecha);

		double precio_apertura = Double.parseDouble(data[1].replace("\"", "")); 
		double precio_maximo = Double.parseDouble(data[2].replace("\"", "")); 
		double precio_minimo = Double.parseDouble(data[3].replace("\"", ""));
		double precio_cierre = Double.parseDouble(data[4].replace("\"", ""));		
		double volumen = Double.parseDouble(data[5].replace("\"", ""));
		
		CotizacionFormateada cotizacion = new CotizacionFormateada();
		cotizacion.setFecha(fechaFormateada);
		cotizacion.setPrecio_apertura(precio_apertura);
		cotizacion.setPrecio_maximo(precio_maximo);
		cotizacion.setPrecio_minimo(precio_minimo);
		cotizacion.setPrecio_cierre(precio_cierre);
		cotizacion.setVolumen(volumen);
				
		return cotizacion;
	}

}