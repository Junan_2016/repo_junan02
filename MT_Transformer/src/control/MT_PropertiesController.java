package control;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class MT_PropertiesController {

	static private Properties vProperties; 
	
    static {
    	try {
    		InputStream is = new MT_PropertiesController().getClass().getResourceAsStream("/MT.properties");
    		vProperties = new Properties();
    		vProperties.load(is);
	    } catch (IOException e) {
			System.out.println("Error la cargar el fichero de propiedades: " + e.getMessage());
		}
    }

    public static final String ARCHIVO_CSV_META = vProperties.getProperty("ARCHIVO_CSV_META");
 
    public static final String RUTA_CARPETA_BAJADAS_CSV = vProperties.getProperty("RUTA_CARPETA_BAJADAS_CSV");
    
}
