package test;

import static org.junit.Assert.*;
import java.util.List;
import org.junit.BeforeClass;
import org.junit.Test;
import pojo.CotizacionFormateada;
import control.FormatearCSVController;

public class FormatearHistoricoTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void test() {
		List<CotizacionFormateada> lista = FormatearCSVController.formatearCSV();
		assertEquals(true, lista.size() > 0);
	}

}
