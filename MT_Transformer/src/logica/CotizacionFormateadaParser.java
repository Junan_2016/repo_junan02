package logica;

import pojo.CotizacionFormateada;

public class CotizacionFormateadaParser {
  
	public static CotizacionFormateada parseEntry(String[] data) {
		
		String fecha = null;
		fecha = data[0].replace("-", ".");
		fecha = fecha.replace("\"", "");		
		double precio_apertura = Double.parseDouble(data[1].replace("\"", "")); 
		double precio_maximo = Double.parseDouble(data[2].replace("\"", "")); 
		double precio_minimo = Double.parseDouble(data[3].replace("\"", ""));
		double precio_cierre = Double.parseDouble(data[4].replace("\"", ""));
		double volumen = Double.parseDouble(data[5].replace("\"", ""));
		
		CotizacionFormateada cotizacionFormateada = new CotizacionFormateada();
		cotizacionFormateada.setFecha(fecha);
		cotizacionFormateada.setHora("00:00:00");
		cotizacionFormateada.setPrecio_apertura(precio_apertura);
		cotizacionFormateada.setPrecio_maximo(precio_maximo);
		cotizacionFormateada.setPrecio_minimo(precio_minimo);
		cotizacionFormateada.setPrecio_cierre(precio_cierre);
		cotizacionFormateada.setVolumen(volumen);
				
		return cotizacionFormateada;
	}

}