package logica;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pojo.CotizacionFormateada;
import util.MetodosFechas;
import control.RY_PropertiesController;

public class FormatearCSV_YH_Logica {

	public static List<CotizacionFormateada> formatCSVforRY() {
		
		String csvFile = RY_PropertiesController.RUTA_CARPETA_BAJADAS_CSV + RY_PropertiesController.ARCHIVO_CSV_RY;
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		
		List<CotizacionFormateada> cotizaciones =  new ArrayList<CotizacionFormateada>();
		List<CotizacionFormateada> cotizacionesASC = new ArrayList<CotizacionFormateada>();
		
		try {
			
			br = new BufferedReader(new FileReader(csvFile));
			
			int band = 0;
						
			while ((line = br.readLine()) != null) {				
			    //use comma as separator
				String[] datoslinea = line.split(cvsSplitBy);
				
				band++;
				if (band != 1){
					
					//array.add(datoslinea);					
					CotizacionFormateada cotizacionFormateada = CotizacionFormateadaParser.parseEntry(datoslinea);					
					cotizaciones.add(cotizacionFormateada);
				}
			}
						
			//Control ordenamiento ASC
			long coti_first = MetodosFechas.crearFechaString(cotizaciones.get(0).getFecha().split("-")).getTimeInMillis(); 
			long coti_last = MetodosFechas.crearFechaString(cotizaciones.get(cotizaciones.size()-1).getFecha().split("-")).getTimeInMillis();
			
			if(coti_first > coti_last){
				int sizeList = cotizaciones.size();
				for(int k=sizeList-1; k>0;k--){
					if(cotizaciones.get(k).getVolumen() >0 ){
						cotizacionesASC.add(cotizaciones.get(k));	
					}
				}
				cotizaciones = cotizacionesASC;
			}

			
			//Control de volumenes 0
			int count = 0;
			for(CotizacionFormateada coti : cotizaciones){
				if(coti.getVolumen() == 0){
					coti.setVolumen(cotizaciones.get(count-1).getVolumen());
					System.out.println("Volumen 0 recargado en " + coti);
				}
				count++;
			}
						
			String[] nombreArchivo = RY_PropertiesController.ARCHIVO_CSV_RY.split("\\.");

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			
			final String rutaArchivoCreado = RY_PropertiesController.RUTA_CARPETA_BAJADAS_CSV + "hist_" + nombreArchivo[0] + "_" + formatter.format(new Date()) + ".csv";
			escribirFicheroCsv(rutaArchivoCreado, cotizaciones);
			
			System.out.println("Nuevo archivo CSV creado en:" + rutaArchivoCreado);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
				
/*		
		System.out.println("size: " + cotizaciones.size());
		System.out.println("Done");
*/
		return cotizaciones;
	}
	
	
	public static void escribirFicheroCsv(String fichero_a_escribir, List<CotizacionFormateada> cotizaciones)
		    throws UnsupportedEncodingException, FileNotFoundException, IOException{
		
		    OutputStream fout = new FileOutputStream(fichero_a_escribir);

		    //para ficheros con símbolos propios del español,
		    //utilizar la codificación "ISO-8859-1"
		    OutputStreamWriter out = new OutputStreamWriter(fout, "UTF8");
		    
		    out.write("fecha,apertura,maximo,minimo,cierre,volumen,openint\n");

	        for(CotizacionFormateada cotizacion: cotizaciones){
		            out.write("\"" + cotizacion.getFecha() + "\",");
		            out.write("\"" + String.valueOf(cotizacion.getPrecio_apertura()) + "\",");
		            out.write("\"" + String.valueOf(cotizacion.getPrecio_maximo()) + "\",");
		            out.write("\"" + String.valueOf(cotizacion.getPrecio_minimo()) + "\",");
		            out.write("\"" + String.valueOf(cotizacion.getPrecio_cierre()) + "\",");		            
		            out.write("\"" + String.valueOf(cotizacion.getVolumen()) + "\",");
		            out.write("\"" + cotizacion.getOpenint() + "\"");
		            out.write("\n");
		    }
		    out.close();
		    fout.close();
	}

}
