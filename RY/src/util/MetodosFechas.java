package util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MetodosFechas {

	/**
	 * @param listaCotizaciones
	 * @param j
	 * @return
	 */
	public static String Date_to_String(Date fecha) {
		return new SimpleDateFormat("yyyy-MM-dd").format(fecha);
	}
	
	public static String Date_to_String(Calendar fechaString) {
		//fechaString.add(Calendar.MONTH, -1);
		return new SimpleDateFormat("yyyy-MM-dd").format(fechaString.getTime());
	}

	/**
	 * Formato String YYYY-MM-dd
	 * @param fechaArray
	 * @return
	 */
	public static Calendar crearFechaString(String[] fechaArray) {
		Calendar vCalendar = Calendar.getInstance(); 
		vCalendar.set(
				Integer.parseInt(fechaArray[0]),
				Integer.parseInt(fechaArray[1])-1,
				Integer.parseInt(fechaArray[2]),
				0,0,0
	    );
		return vCalendar;
	}

	/**
	 * Extrae array de int con yyyy-MM-dd 
	 * @param fecha (yyyy-MM-dd)
	 * @return
	 */
	public static int[] extraeMesFecha(Date fecha){
		
		String[] fecha_String = Date_to_String(fecha).split("-");
		
		int[] fecha_int = new int[3]; 
		
		fecha_int[0] = Integer.parseInt(fecha_String[0]); 
		fecha_int[1] = Integer.parseInt(fecha_String[1]);
		fecha_int[2] = Integer.parseInt(fecha_String[2]);
		
		return fecha_int;
	}	
	
}
