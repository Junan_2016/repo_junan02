package control;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class RY_PropertiesController {

	static private Properties vProperties; 
	
    static {
    	try {
    		InputStream is = new RY_PropertiesController().getClass().getResourceAsStream("/RY.properties");
    		vProperties = new Properties();
    		vProperties.load(is);
	    } catch (IOException e) {
			System.out.println("Error la cargar el fichero de propiedades: " + e.getMessage());
		}
    }

    public static final String ARCHIVO_CSV_RY = vProperties.getProperty("ARCHIVO_CSV_RY");
 
    public static final String RUTA_CARPETA_BAJADAS_CSV = vProperties.getProperty("RUTA_CARPETA_BAJADAS_CSV");
    
}
