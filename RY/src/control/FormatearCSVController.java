package control;

import java.util.List;

import logica.FormatearCSV_YH_Logica;
import pojo.CotizacionFormateada;

public class FormatearCSVController {
	
	public static List<CotizacionFormateada> formatearCSV(){
		
		List<CotizacionFormateada> lista = FormatearCSV_YH_Logica.formatCSVforRY();
		
		return lista;
	}

}
